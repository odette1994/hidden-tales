﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif

public class CloudBuildSettings : ScriptableObject
{
	public DefaultAsset mDevelopmentEntitlementsFile;
    public DefaultAsset mProductionEntitlementsFile;

    private static int _buildNumberDetail = 0;

#if UNITY_CLOUD_BUILD
    public static void PreExport(UnityEngine.CloudBuild.BuildManifestObject manifest)
	{
		Debug.Log("[UCB] PreExport");
		_buildNumberDetail = int.Parse(manifest.GetValue("buildNumber", "0"));
		string buildIdentifier = $"1.{_buildNumberDetail}";
#if UNITY_IOS
		PlayerSettings.iOS.buildNumber = buildIdentifier;
#elif UNITY_ANDROID
		PlayerSettings.Android.bundleVersionCode = _buildNumberDetail; 
#endif
		PlayerSettings.bundleVersion = buildIdentifier;
    }

	public static void OnPostprocessBuildiOS(string exportPath)
	{
#if UNITY_IOS
        ProcessPostBuild(exportPath);
#endif
	}

	public static void ProcessPostBuild(string pathToBuiltProject)
	{
#if UNITY_IOS
        string projPath = pathToBuiltProject + "/Unity-iPhone.xcodeproj/project.pbxproj";

		PBXProject proj = new PBXProject();
		proj.ReadFromString(File.ReadAllText(projPath));

		//Main
		string target = proj.GetUnityMainTargetGuid();
		proj.SetBuildProperty(target, "ENABLE_BITCODE", "NO");

		// Unity Tests
        target = proj.TargetGuidByName(PBXProject.GetUnityTestTargetName());
        proj.SetBuildProperty(target, "ENABLE_BITCODE", "NO");

        // Unity Framework
        target = proj.GetUnityFrameworkTargetGuid();
        proj.SetBuildProperty(target, "ENABLE_BITCODE", "NO");

		proj.WriteToFile(projPath);

		try
		{
			var plistPath = Path.Combine(pathToBuiltProject, "Info.plist");

			var plist = new PlistDocument();
			plist.ReadFromString(File.ReadAllText(plistPath));
			plist.root.SetBoolean("ITSAppUsesNonExemptEncryption", false);
			//plist.root.SetBoolean("UIRequiresFullScreen", true);
			//plist.root.SetBoolean("GADIsAdManagerApp", true);
			//plist.root.SetString("GADApplicationIdentifier", "ca-app-pub-5490273267502119~4024048297");
			//plist.root.SetBoolean("FirebaseMessagingAutoInitEnabled", false);

			if (!plist.root.values.ContainsKey("CFBundleURLTypes"))
			{
				plist.root.CreateArray("CFBundleURLTypes");
			}

			var urlTypes = plist.root.values["CFBundleURLTypes"] as PlistElementArray;

			var getSchemesArray = new PlistElementArray();
			//getSchemesArray.AddString("fb1222306217928190");
			//getSchemesArray.AddString("jeanbobgatorescape");
			//getSchemesArray.AddString("jeanbobgatorescape.game");

			// remove exit on suspend if it exists.
		    string exitsOnSuspendKey = "UIApplicationExitsOnSuspend";
		    if(plist.root.values.ContainsKey(exitsOnSuspendKey))
		    {
			   plist.root.values.Remove(exitsOnSuspendKey);
		    }

			PlistElementDict getSchemeElem = urlTypes.AddDict();
			getSchemeElem.SetString("CFBundleURLName", "com.swanprincess.hiddenobjects"); // PlayerSettings.bundleIdentifier
			getSchemeElem.values["CFBundleURLSchemes"] = getSchemesArray;

			plist.WriteToFile(plistPath);
		}
		catch (System.Exception E)
		{
			Debug.LogError(E.ToString());
		}
#endif
	}
#endif
}
