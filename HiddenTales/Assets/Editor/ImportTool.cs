﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;


public class ImportTool : MonoBehaviour {

	public const string objectsFolder = "Assets/Images/Obj";

	private static List<string> names;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	[MenuItem("Tools/Import Objects")]
	public static void Import()
	{

		//encontra cada objeto separadamente
		names = FindObjs();

		//itera em cada objeto encontrado e encontra as diferentes texturas
		foreach (var item in names) {


			Dictionary<string, List<Sprite>> files = FindTextures(item);

			foreach(KeyValuePair<string, List<Sprite>> pair in files)
			{
				//cria o prefab
				CreateAsset(pair.Key, pair.Value);
			}


		}






	}

	/// <summary>
	/// Cria o asset final
	/// </summary>
	/// <param name="objectName">Object name.</param>
	/// <param name="sprites">Sprites.</param>
	public static void CreateAsset(string objectName, List<Sprite> sprites)
	{

		string finalPath = "Assets/Resources/Obj/"+objectName+".prefab";
		Sprite gameSprite = null;
		Sprite shadowSprite = null;
		Sprite hudSprite = null;

		Debug.Log(objectName + "/" + sprites.Count);

		//atribui os sprites corretos
		foreach (var item in sprites) {
			if(item == null)
				continue;
			
			if(item.name.Contains("game"))
				gameSprite = item;
			else
				if(item.name.Contains("shadow"))
					shadowSprite = item;
				else
					if(item.name.Contains("hud"))
						hudSprite = item;
		}

		//Verifica se o asset já existe na Resources
		GameObject go = AssetDatabase.LoadAssetAtPath(finalPath, typeof(GameObject)) as GameObject;

		SpriteRenderer rend = null;

		HiddenObject hidden = null;

		if(go != null)
		{
			Debug.Log("encontrado " + go.name);

			AssetDatabase.StartAssetEditing();

			rend = go.GetComponent<SpriteRenderer>();


			//Destroy(go.GetComponent<PolygonCollider2D>());

			//substitui texture
			rend.sprite = gameSprite;
			rend.sortingOrder = 1;

			//PolygonCollider2D col = go.GetComponent<PolygonCollider2D>();

			//substitui collider


			hidden = go.GetComponent<HiddenObject>();
			hidden.shadowSprite = shadowSprite;
			hidden.hudSprite = hudSprite;
			hidden.gameSprite = gameSprite;
			//hidden.prefabName = objectName;

			AssetDatabase.StopAssetEditing();
			AssetDatabase.SaveAssets();
			AssetDatabase.SaveAssets();

			return;
		}

		Debug.Log("nao encontrado " + finalPath);

		go = new GameObject();
		go.name = objectName;

		//go.layer = LayerMask.NameToLayer("Survivors");

		//go.tag = "Survivor";

		//--Adiciona componentes

		//Hidden Object
		hidden = go.AddComponent<HiddenObject>();

		//hidden.prefabName = objectName;

		//Sprite Renderer
		rend = go.AddComponent<SpriteRenderer>();
		rend.sortingOrder = 1;

		rend.sprite = gameSprite;

		hidden.shadowSprite = shadowSprite;
		hidden.hudSprite = hudSprite;
		hidden.gameSprite = gameSprite;

		go.AddComponent<PolygonCollider2D>();

		//Cria prefab
		PrefabUtility.CreatePrefab(finalPath, go);

		//remove objeto da cena
		DestroyImmediate(go);


	}


	private static List<string> FindObjs()
	{
		List<string> found = new List<string>();

		string name = string.Empty;

		string[] s = AssetDatabase.FindAssets("t:texture", new string[1] { objectsFolder });

		for (int i = 0; i < s.Length; i++) {


			string path = AssetDatabase.GUIDToAssetPath(s[i]);
			string[] split = path.Split('/');

			name = split[split.Length - 1];

			//deixa apenas o nome do asset, sem a identificacao dele ser um hud, imagem ou sobra
			split = name.Split(new char[2] {'_', '.'});

			name = split[0]+'_'+split[1]+'_'+split[2]+'_'+split[3];

			//Debug.Log(name);

			if(!found.Contains(name))
				found.Add(name);


		}

		//Debug.Log(found.Count);

		return found;
	}


	private static Dictionary<string, List<Sprite>> FindTextures(string name)
	{
		Dictionary<string, List<Sprite>> found = new Dictionary<string, List<Sprite>>();

		string[] s = AssetDatabase.FindAssets("t:texture", new string[1] { objectsFolder });

		List<Sprite> list = new List<Sprite>();

		foreach (var item in s) {

			string path = AssetDatabase.GUIDToAssetPath(item); //transforma em path

			string[] split = path.Split('/');

			string assetName = split[split.Length-1];

			//se não for o asset, ignora
			if(assetName.Contains(name) == false)
				continue;

			Sprite _sprite = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(item), typeof(Sprite)) as Sprite;

			list.Add(_sprite);

		}

		found.Add(name, list);

		//foreach(KeyValuePair<string, List<Sprite>> pair in found)
		//{
		//	Debug.Log( pair.Key + " / " + pair.Value.Count);
		//}


		return found;
	}
}
