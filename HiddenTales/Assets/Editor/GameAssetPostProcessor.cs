﻿using UnityEngine;
using UnityEditor;
using System.Collections;

//My Night Job PostProcessor file, custom import assets
public class GameAssetPostProcessor : AssetPostprocessor
{
	void OnPreprocessTexture()
	{
		
		TextureImporter importer = assetImporter as TextureImporter;
		TextureImporterSettings set = new TextureImporterSettings();
		string name = importer.assetPath.ToLower().Replace('\\', '/');
		string[] ignoreList = new string[]{
			"none"
		};

		bool canCheck = true;
		foreach (string str in ignoreList)
		{
			if (name.StartsWith(str))
				canCheck = false;
		}

		if (canCheck)
		{
			#region Imports For obj
			if (name.StartsWith("assets/images/obj"))
			{
				importer.spritePackingTag = "obj";
				importer.mipmapEnabled = false;
				importer.filterMode = FilterMode.Bilinear;
				importer.maxTextureSize = 256;
				importer.textureFormat = TextureImporterFormat.AutomaticCrunched;

				importer.ReadTextureSettings(set);
				set.spriteAlignment = (int)SpriteAlignment.Center;
				importer.SetTextureSettings(set);
			}
			#endregion
			#region Imports For BGs
			else if (name.StartsWith("assets/resources/bg"))
			{
				importer.spritePackingTag = "bg";
				importer.mipmapEnabled = false;
				importer.filterMode = FilterMode.Bilinear;
				importer.maxTextureSize = 1024;
				importer.textureFormat = TextureImporterFormat.AutomaticCrunched;

				importer.ReadTextureSettings(set);
				set.spriteAlignment = (int)SpriteAlignment.BottomLeft;
				importer.SetTextureSettings(set);
			}
			#endregion

		}
	}
}