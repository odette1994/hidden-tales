﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class ClearPlayerPrefsEditor : MonoBehaviour {

	[MenuItem("Tools/Clear Progress")]
	public static void ClearPlayerPrefs()
	{
		PlayerPrefs.DeleteKey("levels");
		PlayerPrefs.Save();
	}

	[MenuItem("Tools/Clear All PlayerPrefs")]
	public static void ClearAllPlayerPrefs()
	{
		PlayerPrefs.DeleteAll();
		PlayerPrefs.Save();
	}
}
