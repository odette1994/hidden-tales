using UnityEngine;
using System.Collections;

[System.Serializable]
public class HiddenObjectProperties
{
	public string prefabName = "Default Name";
	public Vector2 pos;
	public Vector3 scale;
	public Vector3 uiScale;
	public int sortOrder;
	public bool isImportant;
	//public bool isFound;
	public bool isShadowSpriteActive;
	public bool isLocked;
	public bool isBoss;
	public bool isBogg;
}

public class HiddenObject : MonoBehaviour {

	public System.Action OnDestroyed;


	//public string prefabName = "Default Name";
	//public bool isClickable = true;
	//public bool isImportant {get; private set;}
	//public bool isFound;
	//public bool isShadowSpriteActive;
	public HiddenObjectProperties properties {get; private set;}

	public HiddenObjectUI objectUI;

	private SpriteRenderer spriteRenderer;
	private SpriteRenderer flashSpriteRenderer;
	private PolygonCollider2D c_collider2D;

    public Sprite gameSprite;
	public Sprite hudSprite;
	public Sprite shadowSprite;

    public Sprite GameSprite => gameSprite != null ? gameSprite : hudSprite;
    public Sprite ShadowSprite => shadowSprite != null ? shadowSprite : GameSprite;

    public bool isFound{get; private set;}
	public bool isClickable {get; private set;}
	public bool isBlinking {get; private set;}
	public bool isActivated {get; private set;}

	public static ParticleSystem particle {get; private set;}

	static Material flashMaterial;

	Rigidbody2D rigid;

	// Use this for initialization
	void Awake () {

		spriteRenderer = GetComponent<SpriteRenderer>();
		//gameSprite = spriteRenderer.sprite;

	}

	void OnEnable()
	{
		properties = new HiddenObjectProperties();
		FixObjName();


	}

	/// <summary>
	/// inclui o objeto na UI e GameManager
	/// </summary>
	public void Init()
	{
		
		EnableObjShadow(properties.isShadowSpriteActive);

		//configura o flash
		GameObject go = new GameObject();

		flashSpriteRenderer = go.AddComponent<SpriteRenderer>();

		flashSpriteRenderer.sortingOrder = spriteRenderer.sortingOrder+1;

		go.transform.SetParent(transform, false);

		//cria um sprite novo para o flash
		if(flashMaterial == null)
			flashMaterial = Resources.Load<Material>("Materials/ColorFlash");
		flashSpriteRenderer.material = new Material(flashMaterial);
		flashSpriteRenderer.sprite = GameSprite;

		//desabilita o flash
		flashSpriteRenderer.enabled = false;

		c_collider2D = GetComponent<PolygonCollider2D>();
		gameObject.isStatic = true;

		rigid = gameObject.AddComponent<Rigidbody2D>();
		rigid.isKinematic = true;

		//LeanTween.rotateAround( gameObject, Vector3.forward, -360f, 5f);
		//LeanTween.move( gameObject, gameObject.transform.position + new Vector3(1.7f, 0f, 0f), 2f).setEase(LeanTweenType.easeInQuad);


	}

	public void CreateUI()
	{
		if(objectUI != null)
			return;
		
		//auto-inclui na game UI
		objectUI = GameUI.instance.CreateObjUI(this);

		if(properties.uiScale.magnitude > 0)
			objectUI.transform.localScale = properties.uiScale;

		isActivated = true;
		
	}


	/// <summary>
	/// Faz com que o objeto pisque
	/// </summary>
	public bool Blink()
	{
		if(isFound || !isClickable || objectUI == null)
			return false;
		
		if(objectUI.transform.GetSiblingIndex() > 5) //Somente os clicaveis podem piscar
			return false;

		StopAllCoroutines();

		LeanTween.cancel(flashSpriteRenderer.gameObject);
		LeanTween.scaleY(flashSpriteRenderer.gameObject, 1f, 0);
		LeanTween.scaleX(flashSpriteRenderer.gameObject, 1f, 0);
		
		StartCoroutine(BlinkCo(2.5f, 0, 1, false, ()=>
		{
			StartCoroutine(BlinkCo(2.5f, 1, 0, true, null));
		}));
		
		StartCoroutine (ShakeIt(4.6f));
		return true;

	}

	private IEnumerator ShakeIt(float duration)
	{

		LeanTween.scaleY(flashSpriteRenderer.gameObject, 1.1f, 0.2f).setLoopPingPong();
		LeanTween.scaleX(flashSpriteRenderer.gameObject, 1.1f, 0.2f).setDelay(0.2f).setLoopPingPong();

		yield return new WaitForSeconds(duration);

		LeanTween.cancel(flashSpriteRenderer.gameObject);

		//volta ao estado normal
		LeanTween.scaleY(flashSpriteRenderer.gameObject, 1f, 0.1f);
		LeanTween.scaleX(flashSpriteRenderer.gameObject, 1f, 0.1f);

	}

	private IEnumerator BlinkCo(float duration, float start, float end, bool disableObj, System.Action callBack)
	{
		isBlinking = true;

		float timer = 0;

		if(flashSpriteRenderer == null)
			yield break;

		flashSpriteRenderer.enabled = (true);
		while (timer < duration) {

			timer += Time.deltaTime;

			float normalizedTime = timer/duration;

			float effect = Mathfx.Lerp(start, end, normalizedTime);

			flashSpriteRenderer.material.SetFloat("_FlashAmount", effect);

			yield return null;
		}

		//flashSpriteRenderer.material.SetFloat("_FlashAmount", 0);

		if (disableObj)
			flashSpriteRenderer.enabled = false;
		
		isBlinking = false;

		if (callBack != null)
			callBack();

		yield return null;
	}

	/// <summary>
	/// Checa se este objeto na cena é válido para click
	/// </summary>
	/// <returns><c>true</c> if this instance is valid; otherwise, <c>false</c>.</returns>
	public bool IsValid()
	{
		//Verifica se ele é um dos 6 primeiros objetos da UI ou se ele é clicavel (faz parte da cena)
		if((!isClickable || objectUI.transform.GetSiblingIndex() > 5) && !properties.isBoss && !properties.isBogg){
			return false;
		}

		return true;
	}

	/// <summary>
	/// O objeto foi encontrado pelo jogador
	/// </summary>
	public void OnClick()
	{
		//Verifica se está em modo game
		//if(GameManager.currentGameMode != GameMode.GAME
		//	|| GameManager.instance.currentGameState != GameState.IN_GAME || GameManager.instance.isPaused)
		//		return;


		//remove a UI
		Destroy(objectUI.gameObject);

		StopAllCoroutines();

		flashSpriteRenderer.enabled = false;
		if(properties.isImportant || properties.isBoss || properties.isBogg)
			SoundManager.PlaySFX("sfx02");
		else
			SoundManager.PlaySFX("sfx03");

		isFound = true;

		isClickable = false;

		//Desabilita o collider
		c_collider2D.enabled = false;

		//altera o sprite para o sprite do hud
		spriteRenderer.sprite = hudSprite;

		//altera o layer para frente
		spriteRenderer.sortingOrder = 35;

		//spawna particula
		if(particle == null)
			particle = Resources.Load<ParticleSystem>("Prefabs/Particle");

		ParticleSystem _particle = Instantiate(particle, transform.position, transform.rotation) as ParticleSystem;
		_particle.transform.SetParent(transform);
		_particle.Stop();

		GameObject rotator = new GameObject("rotator"+Random.Range(0,1024).ToString());
		rotator.transform.position = transform.position + new Vector3(7,0,0);

		transform.SetParent(rotator.transform);

		//guarda a escala original
		float scale = gameObject.transform.localScale.x;

		//bounce
		LeanTween.scaleX(gameObject, scale*1.4f, 0.5f).setEase(LeanTweenType.easeOutElastic);
		LeanTween.scaleY(gameObject, scale*1.8f, 0.25f).setEase(LeanTweenType.easeOutSine);
		LeanTween.scaleY(gameObject, scale*1.4f, 0.25f).setEase(LeanTweenType.easeOutSine).setDelay(0.25f);

		//volta ao normal
		LeanTween.scaleX(gameObject, scale, 0.5f).setEase(LeanTweenType.easeOutElastic).setDelay(0.5f);
		LeanTween.scaleY(gameObject, scale, 0.5f).setEase(LeanTweenType.easeOutElastic).setDelay(0.5f);

		LeanTween.moveLocalZ(gameObject, 0, 0.3f).setDelay(0.8f).setEase(LeanTweenType.easeSpring).setOnComplete(
			()=>{
				LeanTween.rotateAround(rotator, Vector3.forward, -1080f, 12f);
				LeanTween.rotateAround(gameObject, Vector3.forward, 0, 12f);
			}
		).setOnStart(()=>{_particle.Play();});



		// Alpha Out, and destroy
		LeanTween.alpha(gameObject, 0f, 0.6f).setDelay(3 + 0.4f).setOnComplete(
			()=>{
				_particle.Stop();
				_particle.transform.SetParent(null);
				Destroy(_particle.gameObject, 10);
				//Destroy( rotator ); // destroying parent as well
				rotator.SetActive(false);
			}
		);	



	}
	
	// Update is called once per frame
	//void Update () {
		
	//}

	void OnDestroy()
	{
		//Remove o objeto
		if(objectUI != null)
			Destroy(objectUI);

		if(OnDestroyed != null)
			OnDestroyed();
	}

	public void SetProperties(HiddenObjectProperties properties)
	{
		this.properties = properties;

		//atualiza a posiçao
		transform.position = properties.pos;
		transform.localScale = properties.scale;

		UpdatePosScale();
		spriteRenderer.sortingOrder = properties.sortOrder;
	}

	public void UpdatePosScale()
	{
		//atualiza a posiçao, escala e nome
		properties.pos = transform.position;
		properties.scale = transform.localScale;
		properties.prefabName = transform.name;
		if(objectUI != null){
			properties.uiScale = objectUI.transform.localScale; //ui
		}
	}
    
    public void ChangeSortOrder(int value)
	{
		int val = spriteRenderer.sortingOrder;

		val += value;

		if(val < 1)
			val = 1;
		if(val > 30)
			val = 30;

		properties.sortOrder = val;

		spriteRenderer.sortingOrder = properties.sortOrder;
	}

	public int GetSortOrder
	{
		get{return properties.sortOrder;}
	}

	/// <summary>
	/// Remove o nome "Clone" do objeto
	/// </summary>
	public void FixObjName()
	{
		
		string theName = transform.name;
		//verifica se não existe a palavra clone
		if(theName.Contains("(Clone)"))
		{
			//remove a palavra clone
			theName = theName.Substring(0, theName.Length - 7);
			transform.name = theName;
		}
	}

	public void EnableObjShadow(bool enable)
	{
		//if(shadowSprite == null)
		//	return;

		properties.isShadowSpriteActive = enable;
		spriteRenderer.sprite = enable ? ShadowSprite : GameSprite;
	}

	public void EnableImportance(bool enable)
	{
		properties.isImportant = enable;
	}

	public void EnableBogg(bool enable)
	{
		properties.isBogg = enable;
	}

	public void EnableIsBoss(bool enable)
	{
		properties.isBoss = enable;
	}

	public void SetLocked(bool locked)
	{
		properties.isLocked = locked;
	}

	public void Move(Vector3 direction)
	{
		if(GameManager.currentGameMode > 0)
			return;

		if(properties.isLocked)
			return;
		
		transform.Translate(direction);
		transform.position = KeepOnScreen(transform.position);
	}

	Vector3 KeepOnScreen(Vector3 pos){
		

		if(pos.x >= 19.3f)
			pos.x = 19.3f;
	
			if(pos.x <= 0)
				pos.x = 0;

				if(pos.y >= 12.5f)
					pos.y = 12.5f;
				
					if(pos.y <= 1.7f)
						pos.y = 1.7f;

		return pos;


	}

	Color col;
	/// <summary>
	/// Define a transparencia do objeto.
	/// Manipulada pelo level editor
	/// </summary>
	/// <param name="amout">Amout.</param>
	public void SetTransparency(float amout)
	{
		col = spriteRenderer.material.color;
		col.a = amout;
		spriteRenderer.material.color = col;

	}

	public void SetTransparencyTween(float to, float time)
	{
		LeanTween.alpha(gameObject, to, time);
	}

	public void SetClickable(bool clickable)
	{
		isClickable = clickable;
	}

	public void EnableCollision(bool enable)
	{
		c_collider2D.enabled = enable;
	}

	/// <summary>
	/// Deixa a HUD do objeto em primeiro lugar
	/// </summary>
	public void SetFirstInHUD()
	{
		objectUI.transform.SetSiblingIndex(0);
	}
}
