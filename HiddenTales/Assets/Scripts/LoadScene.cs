﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using PlayerPrefs = PreviewLabs.PlayerPrefs;

public class LoadScene : MonoBehaviour {


//	public RectTransform rect_loading;
	public bool b_canFade = false;     
	public AsyncOperation loadOp;
	public RectTransform Odette;
	public RectTransform LoadingBar;
	public Slider slider_loadindBar;
	public Text gameVersion;
	float fLoading = 0;
	float fLoadingTarget = 0;

	// Use this for initialization
	void Start () {

		UpdateVersionText();

		//		LeanTween.alpha(rect_loading, 0, 0f);
		//		LeanTween.rotate(rect_loading, -360, 2).setLoopClamp();
		//		LeanTween.alpha (Odette, 0f, 1.5f);
		fLoadingTarget = LoadingBar.localScale.x;
		LoadingBar.localScale = new Vector3(0,fLoadingTarget,fLoadingTarget);
		Fade.FadeIn(1,100,
			()=>{
				
//				LeanTween.alpha(rect_loading, 1, 1f).setOnComplete(
//					()=>{
						StartCoroutine(Init());
//					});
					
			});


	}

	IEnumerator Init()
	{
		SoundManager.Init();
		yield return new WaitForEndOfFrame();
		//GameManager.FillObjectsListEditor();
		//yield return new WaitForEndOfFrame();
		//Inicializa os dialoogos
		Dialogs.GetDialog("ccc:");
//		Debug.Log ("My INIT Language: " + Application.systemLanguage);
		yield return new WaitForEndOfFrame();
		//Inicializa o PlayerPrefs
		string prefs = PlayerPrefs.GetString("nnn", "nnn");
		yield return new WaitForEndOfFrame();

		//		LeanTween.alpha(rect_loading, 0, 0.3f);

		LeanTween.scale(LoadingBar, new Vector3(fLoadingTarget * 0.3f, fLoadingTarget, fLoadingTarget), 0.2f);

		yield return new WaitForSeconds(0.3f);




		LeanTween.alpha (Odette, 0f, 1f).setOnComplete(
			()=>{
		LeanTween.scale (LoadingBar, new Vector3 (fLoadingTarget, fLoadingTarget, fLoadingTarget), 1f).setOnComplete(
//		LeanTween.alpha(rect_loading.GetComponent<RectTransform>(), 0, 0.4f).setOnComplete( 
			()=>{

				//Carrega a cena
				Fade.FadeOut(1, 100, ()=>{
					b_canFade = true;
					loadOp = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("Menu");
				});
			});
		
		});

	}
	//void Update()    
	//{   
	//if (b_canFade) {
	//Debug.Log (loadOp.progress.ToString () + "%");
	//}
	//TEMP: trying to see if loadlevelaync works or not...    
	//} 

	private void UpdateVersionText()
	{
		if (ManifestInfo.Instance.BuildVersion != "NotAssigned")
			AppVersionChanged();
		else
			ManifestInfo.Instance.OnVersionUpdated += AppVersionChanged;
	}

	private void AppVersionChanged()
	{
		gameVersion.text = $"v 1.1.{ManifestInfo.Instance.BuildVersion}";
	}
}
