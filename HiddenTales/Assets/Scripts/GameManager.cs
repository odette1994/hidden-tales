using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using System;
using System.Threading.Tasks;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private const int TIME_ATTACK_THRESHOLD = 90;
    public static System.Action<bool> OnLevelLoaded;
    public static System.Action OnObjectFound;
    public static GameManager instance;

    public GameSettings settings { get; private set; }

    public static GameMode currentGameMode = GameMode.EDITOR;//editor mode ou game mode?
    public GameState currentGameState; //ingame, lose, win, pause

    [HideInInspector]
    public List<HiddenObject> List_hiddenObjects = new List<HiddenObject>();

    public SpriteRenderer bgRenderer { get; private set; }

    public int starsCount { get; private set; }
    public int starsCountTimeAttack { get; private set; }
    public int starsCountEasyMode { get; private set; }

    //quantas estrelas o jogador tinha para o personagem quando entrou do jogo
    public static int storedTotalStarsCount { get; set; }
    int starCountStored;
    int starCountStoredTimeAttack;
    int starCountStoredEasyMode;

    public int findsRemain { get; private set; } //numero de lupas restantes

    public int i_ObjectsFoundCount { get; private set; } //numero de objetos encontrados

    //Level editor
    public static Dictionary<int, List<HiddenObject>> hiddenObjectsLvlEditor = new Dictionary<int, List<HiddenObject>>();
    public static List<Sprite> bgs = new List<Sprite>();//lista de bgs
                                                        //--
    [HideInInspector]
    public GameLevelData currentLevelData;

    public float TimeLeft { get; private set; }
    float totalTime;
    public float TimeSpan { get; private set; } //tempo corrido de jogo
    public float timePenalty { get; private set; }
    public float BestTime { get; private set; } //melhor tempo salvo (timeattack)
    private float foundTimer;

    [HideInInspector]
    public GameLevelStisticsData gameLevelStatisticData = new GameLevelStisticsData();

    bool gameOver;
    public bool isPaused { get; private set; }

    private Blur blurEffect;

    //Cooldown para a lupa
    private float findCooldown = 0;

    //Boggs
    private bool levelHasBoggs; //o level possui boggs?
    private float boggsTimer1; //timer para encontrar o boggs #1
    private float boggsTimer2; //timer para encontrar o boggs #2

    private LevelResult currentLevelResult;

    private HiddenObject boggs1;
    private HiddenObject boggs2;

    //Time attack
    public static readonly int minStarsTimeAttack = 1; //estrelas minimas para liberar o time attack

    //Added FakeFlashLight For boss
    public FlashLightBoss flashLightBoss;

    private bool _bgLoading = true;
    private bool _charLoading = true;
    private AsyncOperationHandle<Sprite> _loadedBackground;
    private AsyncOperationHandle<Sprite> _loadedBigChar;

    void Awake()
    {
        instance = this;
        bgRenderer = GameObject.FindGameObjectWithTag("BG").GetComponent<SpriteRenderer>();
        blurEffect = Camera.main.GetComponent<Blur>();
    }

    void Reset()
    {
        boggs1 = null;
        boggs2 = null;
        boggsTimer1 = 0;
        boggsTimer2 = 0;
        levelHasBoggs = false;
        i_ObjectsFoundCount = 0;
        TimeSpan = 0;
    }

    // Use this for initialization
    void Start()
    {
        LoadLevelStatisticsData();

        settings = Utils.GetGameSettings();
        string lvl = Menu.levelToLoad;

        //se o modo for game, inicializa o level
        if (currentGameMode > 0)
        {
            //if(lvl == string.Empty)
            //	lvl = Menu.levelToLoad;
            Load(lvl, currentGameMode);
        }
        //Debug.Log("Estrelas: " + storedStarsCount);
    }

    void OnDisable()
    {
        ReleaseAddressables();
    }


    private void BigCharacterLoaded(AsyncOperationHandle<Sprite> obj)
    {
        _loadedBigChar = obj;
        _charLoading = false;
    }

    static List<HiddenObject> objects = new List<HiddenObject>();
    /// <summary>
    /// Procura objetos para entrarem na lista de objetos para o level editor
    /// </summary>
    public static void FillObjectsListEditor()
    {
        if (objects.Count > 0)
            return;

        //procura por todos os prefabs de objetos
        objects.AddRange(Resources.LoadAll<HiddenObject>("Obj"));

        Debug.Log("Objects count: " + objects.Count);

        foreach (var item in objects)
        {
            //verifica a qual lvl o obj pertence
            int lvl = int.Parse(item.name.Split('_')[1]);

            List<HiddenObject> list = new List<HiddenObject>();

            hiddenObjectsLvlEditor.TryGetValue(lvl, out list);

            if (list == null)
            {
                list = new List<HiddenObject>();
                list.Add(item);
                hiddenObjectsLvlEditor.Add(lvl, list);
            }
            else
                list.Add(item);
        }

        //Log
        foreach (KeyValuePair<int, List<HiddenObject>> pair in hiddenObjectsLvlEditor)
        {
            Debug.Log(pair.Key + " / " + pair.Value.Count);
        }

        //BGs
        bgs.AddRange(Resources.LoadAll<Sprite>("BG"));
        Debug.Log(bgs.Count);
    }


    public void OnGameOver()
    {
        gameOver = true;
    }

    void OnTimeUp()
    {
        if (gameOver)
            return;

        OnGameOver();
        StartCoroutine(OnLevelEnd());
    }

    private void Debug_FinishAndWin()
    {
        for(int i = 0; i < List_hiddenObjects.Count; ++i)
            i_ObjectsFoundCount++;
        List_hiddenObjects.Clear();

        StartCoroutine(OnLevelEnd());
    }

    private void Debug_EditorControls()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //Chama a caixa de confirmação
            if (currentGameMode == GameMode.EDITOR)
            {
                ConfirmationWindow.instance.RiseWindow("Exit Editor?", () =>
                {
                    Debug.Log("Quitting");
                    UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
                });
            }
        }

        if (Input.GetKeyDown(KeyCode.J))
            BlinkObject();

        //Força a liguagem
        if (Input.GetKeyDown(KeyCode.Alpha1))
            Dialogs.SetLanguage(Languages.English);

        if (Input.GetKeyDown(KeyCode.Alpha2))
            Dialogs.SetLanguage(Languages.Portuguese);

        if (Input.GetKeyDown(KeyCode.Alpha3))
            Dialogs.SetLanguage(Languages.German);

        if (Input.GetKeyDown(KeyCode.Alpha4))
            Dialogs.SetLanguage(Languages.Spanish);

        if (Input.GetKeyDown(KeyCode.V))
            Debug_FinishAndWin();
    }

    // Update is called once per frame
    void Update()
    {
        findCooldown -= Time.deltaTime;

        //controla o tempo se o modo for game ou test
        if ((currentGameMode == GameMode.GAME || currentGameMode == GameMode.GAME_EASY) && currentGameState == GameState.IN_GAME && !isPaused)
        {
            UpdateBaseStoryMode();
        }

        //Time attack
        if (currentGameMode == GameMode.GAME_TIMEATTACK && currentGameState == GameState.IN_GAME && !isPaused)
        {
            UpdateTimeAttackGame();
        }

        starCountStored = starsCount;
        starCountStoredTimeAttack = starsCountTimeAttack;
        starCountStoredEasyMode = starsCountEasyMode;

#if UNITY_EDITOR
        Debug_EditorControls();
#endif
    }

    /// <summary>
    /// Processa o Time Attach
    /// </summary>
    void UpdateTimeAttackGame()
    {
        TimeSpan += Time.deltaTime;
        foundTimer += Time.deltaTime;

        if (TimeSpan < TIME_ATTACK_THRESHOLD)
            starsCountTimeAttack = 1;
        else starsCountTimeAttack = 0;

        if (starCountStoredTimeAttack > starsCountTimeAttack)
            SoundManager.PlaySFX("sfx06");
    }

    void UpdateBaseStoryMode()
    {
        float timeScale = 1;

        if (Input.GetKey(KeyCode.Space))
            timeScale = 30;
        else
            timeScale = 1;

        TimeLeft -= Time.deltaTime * timeScale;
        TimeLeft = Mathf.Clamp(TimeLeft, 0, Mathf.Infinity);

        TimeSpan += Time.deltaTime;

        foundTimer += Time.deltaTime;

        if (foundTimer >= settings.f_blinkAfter)
        {
            BlinkObject();
            foundTimer = 0;
        }

        if (currentGameMode == GameMode.GAME)
            UpdateNormalGameStars();
        if (currentGameMode == GameMode.GAME_EASY)
            UpdateEasyGameStars();

        if (TimeLeft <= 0)
            OnTimeUp();


        //Controla os Boggs

        //Ativa Bogg por tempo
        if (levelHasBoggs)
        {
            //Boggs1
            if (!boggs1.isActivated)
            {
                if (TimeSpan > settings.f_first_bogg_appears_after_seconds)
                    ActivateBogg();
            }
            else if (!boggs1.isFound)
            {
                boggsTimer1 += Time.deltaTime;

                if (boggsTimer1 > settings.f_timeToFindBoggs)
                    OnTimeUp();
            }

            //Boggs2
            if (!boggs2.isActivated)
            {
                if (TimeSpan > settings.f_second_bogg_appears_after_seconds)
                    ActivateBogg();
            }
            else if (!boggs2.isFound)
            {
                boggsTimer2 += Time.deltaTime;
                if (boggsTimer2 > settings.f_timeToFindBoggs)
                    OnTimeUp();
            }
        }
    }

    void UpdateEasyGameStars()
    {
        if (TimeLeft <= 1)
            starsCountEasyMode = 0;
        else starsCountEasyMode = 1;

        if (starCountStoredEasyMode > starsCountEasyMode)
            SoundManager.PlaySFX("sfx06");
    }

    /// <summary>
    /// Lógica para processar o jogo normal
    /// </summary>
    void UpdateNormalGameStars()
    {
        //controla a contagem de estrelas
        float normalizedTime = TimeLeft / totalTime;
        if (TimeLeft > 0)
            starsCount = normalizedTime > 0.66f ? 3 : normalizedTime > 0.33f ? 2 : 1;
        else
            starsCount = 0;

        if (starCountStored > starsCount)
            SoundManager.PlaySFX("sfx06");
    }

    /// <summary>
    /// Escolhe um objeto randomicamente para piscar
    /// </summary>
    public void BlinkObject()
    {
        if (List_hiddenObjects.Count <= 0)
            return;

        int rdm = UnityEngine.Random.Range(0, List_hiddenObjects.Count);

        bool canBlink = List_hiddenObjects[rdm].Blink();

        if (!canBlink)
            BlinkObject();
    }

    void ActivateBogg()
    {
        HiddenObject bogg = null;

        if (!boggs1.isActivated)
            bogg = boggs1;
        else
            bogg = boggs2;

        bogg.SetClickable(true);
        bogg.CreateUI();
        bogg.SetTransparencyTween(1, 1);
        bogg.EnableCollision(true);
        bogg.SetFirstInHUD();
    }

    /// <summary>
    /// Clicou errado no BG ou no objeto (penaliza)
    /// </summary>
    public void OnErrorClick()
    {
        if (currentGameState != GameState.IN_GAME)
            return;

        if (isPaused)
            return;

        if (currentGameMode == GameMode.GAME)
        {
            //desconta o timeleft
            TimeLeft -= settings.f_timePenaltyError;
        }
        else if (currentGameMode == GameMode.GAME_EASY)
        {
            TimeLeft -= settings.f_timePenaltyErrorEasyMode;
        }
        else
        {
            TimeSpan += settings.f_timePnaltyErrorTimeAttack;
        }

        SoundManager.PlaySFX("error");
    }

    public void OnFindObject(HiddenObject obj)
    {
        if (isPaused)
            return;

        if (!obj.properties.isBogg) //Boggs não conta
            i_ObjectsFoundCount++;

        foundTimer = 0;
        List_hiddenObjects.Remove(obj);
        bool enableBoss = true;

        //habilita boss?
        foreach (var item in List_hiddenObjects)
        {
            if (!item.properties.isBoss)
            {
                enableBoss = false;
                break;
            }
        }

        if (enableBoss)
        {
            ActivateBoss();
        }

        //Ativa o Boggs?
        //Boggs 1
        if (levelHasBoggs)
        {
            if (!boggs1.isActivated && i_ObjectsFoundCount >= settings.i_first_bogg_appears_after_objects)
                ActivateBogg();

            //Boggs2
            if (!boggs2.isActivated && i_ObjectsFoundCount >= settings.i_second_bogg_appears_after_objects)
                ActivateBogg();
        }

        if (List_hiddenObjects.Count <= 0)
        {
            StartCoroutine(OnLevelEnd());
        }

        //Evento
        OnObjectFound();

        //Achievement de objetos encontrados
        int objCount = PlayerPrefs.GetInt("objfound", 0);

        objCount++;
        PlayerPrefs.SetInt("objfound", objCount);

        Debug.Log("total objects found: " + objCount);
    }

    public int GetObjectsRemaining()
    {
        return List_hiddenObjects.Count;
    }

    void ActivateBoss()
    {
        bool isBossActivated = false;
        foreach (var item in List_hiddenObjects)
        {
            if (item.properties.isBoss)
            {
                item.SetClickable(true);
                item.CreateUI();
                item.SetTransparencyTween(1, 1);
                item.EnableCollision(true);
                isBossActivated = true;
            }
        }

        if (isBossActivated)
            flashLightBoss.StartFlashLight();
    }

    private void SubmitAnalyticsStats(GameLevelStatistics stats, GameMode mode)
    {
        MyAnalytics.Instance.LogEvent($"{mode}",$"Level: {stats.levelID}", $"{stats.levelResult}", 1);
    }

    IEnumerator OnLevelEnd()
    {
        currentGameState = GameState.LEVEL_END;

        flashLightBoss.StopFlashLight();

        //Para o time attack, segue para o final alternativo
        if (currentGameMode == GameMode.GAME_TIMEATTACK)
        {
            StartCoroutine(OnTimeAttackLevelEnd());
            yield break;
        }

        bool win = false;

        //Será salvo no Playerprefs
        GameLevelStatistics statistic = new GameLevelStatistics();
        statistic.levelID = currentLevelData.levelID;
        statistic.character = (int)ParseEnum<Characters>(currentLevelData.character);

        //Venceu ou perdeu o level

        win = List_hiddenObjects.Count <= 0 && TimeLeft > 0;

        if (!win)
            statistic.levelResult = LevelResult.TIMEUP;

        if (win && !levelHasBoggs)
        {
            statistic.levelResult = LevelResult.WIN;
        }
        else
        {
            if (levelHasBoggs)
            {
                win &= boggs1.isFound && boggs2.isFound;

                if (!win)
                {
                    if (boggsTimer1 <= 0 && boggsTimer2 <= 0)
                        statistic.levelResult = LevelResult.TIMEUP;
                    else
                    {
                        if ((boggs1.isActivated && !boggs1.isFound) || (boggs2.isActivated && !boggs2.isFound))
                        {
                            statistic.levelResult = LevelResult.BOGG_TIMEUP;
                            hasShowedAlternativeIntro = false;
                        }
                    }
                }
            }
        }

        if (win)
        {
            statistic.levelResult = LevelResult.WIN;
            if (currentGameMode == GameMode.GAME)
            {
                if (starsCount > gameLevelStatisticData.GetLevelStatistics(currentLevelData.levelID).bestStar)
                    statistic.bestStar = starsCount;
            }

            if(currentGameMode == GameMode.GAME_EASY)
            {
                if (starsCountEasyMode > gameLevelStatisticData.GetLevelStatistics(currentLevelData.levelID).bestStar)
                    statistic.bestStar = starsCountEasyMode;
            }
        }
        else
        {
            starsCount = 0;
            starsCountEasyMode = 0;
            starsCountTimeAttack = 0;
        }

        //---------

        //Habilita o blur imediatamente quando perde
        //if(!win)
        blurEffect.enabled = true;

        if(currentGameMode == GameMode.GAME)
            statistic.starsCount = starsCount;
        if (currentGameMode == GameMode.GAME_EASY)
            statistic.starsCount = starsCountEasyMode;

        SubmitAnalyticsStats(statistic, currentGameMode);

        Debug.Log("stars count: " + starsCount);

        string scn = statistic.levelID.Split('_')[1];

        string log = Utils.GetCharacterName(statistic.character) + "_" + scn + "_" + statistic.starsCount.ToString();

        Debug.Log(log);

        //Verifica se é a primeira vez que o time attack foi liberado neste level

        bool willShowTimeAttack = gameLevelStatisticData.GetLevelStatistics(currentLevelData.levelID).starsCount < minStarsTimeAttack;
        willShowTimeAttack &= starsCount >= minStarsTimeAttack;

        //Salva as estrelas ganhas no PlayerPrefs
        bool newBest;
        gameLevelStatisticData.SetLevelStatistic(statistic, out newBest);

        SaveStatisticsData();

        PlayerPrefs.Save();

        //stop bgm
        SoundManager.StopBGM();

        //abre o popup
        float t = GameUI.instance.OnWin(currentGameMode == GameMode.GAME_EASY ? starsCountEasyMode : starsCount, statistic.levelResult == LevelResult.BOGG_TIMEUP, currentGameMode);

        yield return new WaitForSeconds(t);

        //mostra dialogos
        CharacterDialog dialog = statistic.levelResult == LevelResult.WIN ? currentLevelData.onWinDialog : currentLevelData.onLoseDialog;
        dialog.character = currentLevelData.character;

        //Diálogos de derrota hardcoded para os levels da cena 07
        if (statistic.levelResult == LevelResult.BOGG_TIMEUP)
        {
            if (currentLevelData.levelID.Contains("st07_sc02"))
            {
                dialog.dialog = "72db";
            }
            else if (currentLevelData.levelID.Contains("st07_sc03"))
            {
                dialog.dialog = "73db";

                //Se perdeu para o boggs, deve voltar para o Menu
                currentLevelData.levelOnLose = "Menu";
            }
            else if (currentLevelData.levelID.Contains("st07_sc04"))
            {
                dialog.dialog = "74db";

            }
            else if (currentLevelData.levelID.Contains("st07_sc05"))
            {
                dialog.dialog = "75db";
            }
        }

        //mostra o time attack
        if (willShowTimeAttack)
        {

            //Loga no GA que conseguiu liberar o timeAttack
            log = Utils.GetCharacterName(statistic.character) + "_" + scn + "_TimeAttackUnlocked";

            GameUI.instance.ShowTimeAttackEnabled(true);
            yield return new WaitForSeconds(4);
            GameUI.instance.ShowTimeAttackEnabled(false);
            yield return new WaitForSeconds(2);
        }

        //Dialogos

        if (GameUI.instance.ShowDialogs(dialog, _loadedBigChar.Result))
        {

            //blurEffect.enabled = true;

            //esconde hud
            GameUI.instance.ShowHud(false, 1);

            yield return new WaitForSeconds(2.1f);//aguarda o dialog de áudio começar


            float dialogTimer = SoundManager.GetDialogClipLength();

            if (dialogTimer <= 0)
                dialogTimer = 4;

            //aguarda o diálogo
            while (dialogTimer > 0 && !GameUI.instance.dialogSkipped)
            {
                dialogTimer -= Time.deltaTime;
                yield return null;
            }

            GameUI.instance.HideDialogs();

            yield return new WaitForSeconds(2);

            //blurEffect.enabled = false;
        }

        //aguarda o input do usuario ou timer
        //		float timer = 0;
        //		bool skip = false;
        //		while (timer < 10 && !skip) {
        //
        //			timer+=Time.deltaTime;
        //
        //			skip = timer > 1.3f && Input.anyKeyDown;
        //
        //			yield return null;
        //		}
        //
        //		GameUI.instance.HideDialogs();
        //
        //		yield return new WaitForSeconds (3);

        string nextLevel = win ? currentLevelData.levelOnWin : currentLevelData.levelOnLose;


        //Complete Level need 15 total of Level
        //Aqui informamos que o jogador terminou o capítulo
        //int levelStartCount = gameLevelStatisticData.GetStarCount(Utils.GetCharaterID(currentLevelData.character));
        //if (levelStartCount >= 15 && storedTotalStarsCount < 15 && nextLevel == "Menu")
        //    ChapterSelect.ShowCompletedLevel((Characters)Utils.GetCharaterID(currentLevelData.character));

        //Need 3x5 Total ( 3 star for each stage )
        int characterID = Utils.GetCharaterID(currentLevelData.character);
        if ( PlayerPrefs.GetInt("complete_charactor_"+ characterID , 0) == 0 && gameLevelStatisticData.GetIsCompleateCharaterLevel(characterID))
        {
            PlayerPrefs.SetInt("complete_charactor_" + characterID, 1);
            ChapterSelect.ShowCompletedLevel((Characters)characterID);
        }
        Debug.Log("----------------------------------------------------------");
        //Debug.Log("levelStartCount:" + levelStartCount);
        Debug.Log("storedTotalStarsCount:" + storedTotalStarsCount);



        //load proximolevel
        Fade.FadeOut(1, 100, () =>
        {
            ReleaseAddressables();
            Load(nextLevel, currentGameMode);
        });

        yield return null;
    }

    int CalculateSwanPoints()
    {
        List<string> allLvels = new List<string>(SaveLoad.GetAllLevels());
        int totalSwans = gameLevelStatisticData.GetStarCount();
        float baseScore = totalSwans * 200;

        //Debug.Log("total swans: " + totalSwans);

        foreach (var item in allLvels)
        {
            if (item == "nnn") //bypass o dummy level
                continue;

            //pega o best time do level
            var stat = gameLevelStatisticData.GetLevelStatistics(item);

            if (stat.bestTime > 0)
                baseScore += 700 - stat.bestTime;

            //Debug.Log("bs " + baseScore);
        }

        return (int)baseScore;
    }

    IEnumerator OnTimeAttackLevelEnd()
    {
        //Habilita o blur imediatamente quando perde
        //if(!win)
        blurEffect.enabled = true;

        //Será salvo no Playerprefs
        var statistic = new GameLevelStatistics();
        statistic.levelID = currentLevelData.levelID;
        statistic.character = (int)ParseEnum<Characters>(currentLevelData.character);
        statistic.bestTime = TimeSpan;
        statistic.starsCount = starsCountTimeAttack;
        bool newRecord;
        //Salva no PlayerPrefs
        gameLevelStatisticData.SetLevelStatistic(statistic, out newRecord);

        SaveStatisticsData();

        PlayerPrefs.Save();
        //stop bgm
        SoundManager.StopBGM();

        GameUI.instance.OnTimeAttackEnd();

        yield return new WaitForSeconds(13);

        //Show New Record
        if (newRecord)
        {
            GameUI.instance.ShowNewRecord();

            yield return new WaitForSeconds(5);
        }

        //load proximolevel
        Fade.FadeOut(1, 100, () =>
        {
            ReleaseAddressables();
            Load("Menu", currentGameMode);
        });

        yield return null;
    }

    private static bool hasShowedAlternativeIntro;

    IEnumerator OnLevelStart()
    {
        //mostra o dialogo de introducao
        Fade.FadeIn(1, 100);

        BestTime = gameLevelStatisticData.GetLevelStatistics(currentLevelData.levelID).bestTime;

        //esconde hud
        GameUI.instance.ShowHud(false, 0);
        GameUI.instance.ResetStarsHud();

        //Game normal
        if (currentGameMode == GameMode.GAME || currentGameMode == GameMode.GAME_EASY)
        {
            //mostra o dialogo de inicio
            CharacterDialog dialog = currentLevelData.introDialog;
            dialog.character = currentLevelData.character;

            //Hardcode de diálogos do estágio 07

            if (currentLevelData.levelID.Contains("st07_sc01"))
            {
                GameLevelStatistics lvl01 = gameLevelStatisticData.GetLevelStatistics("st07_sc01");
                GameLevelStatistics lvl02 = gameLevelStatisticData.GetLevelStatistics("st07_sc02");
                GameLevelStatistics lvl03 = gameLevelStatisticData.GetLevelStatistics("st07_sc03");
                GameLevelStatistics lvl04 = gameLevelStatisticData.GetLevelStatistics("st07_sc04");
                GameLevelStatistics lvl05 = gameLevelStatisticData.GetLevelStatistics("st07_sc05");

                if (!hasShowedAlternativeIntro && lvl01.levelResult != LevelResult.NONE)
                {
                    string dialogID = string.Empty;

                    //Verifica se não perdeu nenhum level
                    if (lvl02.HasLostLevel() || lvl03.HasLostLevel() ||
                        lvl04.HasLostLevel() || lvl05.HasLostLevel())
                    {
                        dialogID = "71ic";
                    }

                    //Verifica se já venceu a fase 01
                    if (lvl01.HasLostLevel())
                        dialogID = "71ib";
                    else
                        if (lvl05.levelResult == LevelResult.WIN || lvl01.levelResult == LevelResult.WIN)
                        dialogID = "71ia";

                    if (lvl05.levelResult == LevelResult.BOGG_TIMEUP)
                        dialogID = "71ic";

                    dialog.dialog = dialogID;
                    hasShowedAlternativeIntro = true;
                }
                else
                {
                    dialog.dialog = "71ia";
                    hasShowedAlternativeIntro = false;
                }
            }
            ///------

            //ativa o blur somente se tiver diálogo
            blurEffect.enabled = dialog.dialog != string.Empty;

            yield return new WaitForSeconds(1);

            while (_charLoading) //We're about to show character dialog. We need to be sure character sprite is loaded
                yield return null;

            if (GameUI.instance.ShowDialogs(dialog, _loadedBigChar.Result))
            {
                SoundManager.StopBGM();
                yield return new WaitForSeconds(2.1f);//aguarda o dialog de áudio começar
                float dialogTimer = SoundManager.GetDialogClipLength();

                if (dialogTimer <= 0)
                    dialogTimer = 4;

                //aguarda o diálogo
                while (dialogTimer > 0 && !GameUI.instance.dialogSkipped)
                {
                    dialogTimer -= Time.deltaTime;
                    yield return null;
                }

                GameUI.instance.HideDialogs();
                yield return new WaitForSeconds(1);
            }
        }
        else
        {
            blurEffect.enabled = true;
            GameUI.instance.OnTimeAttackStart();
            yield return new WaitForSeconds(2);
            blurEffect.enabled = false;
            StartCoroutine(StartGame());
        }
        GameUI.instance.ShowStartButton(true);
    }

    public IEnumerator StartGame()
    {
        //desativa o blur
        blurEffect.enabled = false;

        //Toca o BGM
        SoundManager.PlayBGM(currentLevelData.character);

        //mostra hud
        GameUI.instance.ShowHud(true, 1);

        yield return new WaitForSeconds(2);

        currentGameState = GameState.IN_GAME;

        SetPause(false);

        yield return null;
    }

    public void AddHiddenObject(HiddenObject obj)
    {
        List_hiddenObjects.Add(obj);
    }

    public void Save(string levelID)
    {
        //pega todos os ObjectStatistic
        List<HiddenObjectProperties> list = new List<HiddenObjectProperties>();

        //pega todos os hiddenObject
        foreach (var item in List_hiddenObjects)
        {
            if (item == null)
                continue;

            Debug.Log(item.name);

            item.UpdatePosScale();
            item.FixObjName();//corrige o nome do objeto
            list.Add(item.properties);
        }
        GameLevelData data = new GameLevelData();

        if (currentLevelData != null)
            data = currentLevelData;

        data.objectList = list;
        data.levelID = levelID;

        //bg
        data.bg = bgRenderer.sprite.name;

        //Salva
        SaveLoad.Save(data);
    }

    public void Load(string levelID, GameMode gameMode)
    {
        currentGameMode = gameMode;

        StopAllCoroutines();
        StartCoroutine(LoadCo(levelID));
    }

    IEnumerator LoadCo(string levelID)
    {
        if (levelID.Contains("Menu"))
        {

            UnityEngine.SceneManagement.SceneManager.LoadScene("LevelSelect");
            yield break;
        }

        GameUI.instance.ShowLoading(true, 0);

        //Para o BGM
        SoundManager.StopBGM();
        GameUI.instance.HideDialogs(true);

        //Checa se o level existe
        if (SaveLoad.CheckLevel(levelID) == false)
        {
            Debug.LogWarning("Level " + levelID + " doesn't exist");

            if (levelID == string.Empty)
                Debug.LogWarning("Level is empty");

            OnLevelLoaded(false);
            yield break;
        }

        Fade.Hide();
        gameOver = false;

        //limpa todos os objetos
        ClearAll();

        GameLevelData data = SaveLoad.Load(levelID);
        currentLevelData = data;

        Addressables.LoadAssetAsync<Sprite>(currentLevelData.character).Completed += BigCharacterLoaded; 

        #region instancia todos os objetos do level

        foreach (var item in data.objectList)
        {

            string _name = item.prefabName;

            //verifica se existe clone no nome
            if (_name.Contains("(Clone)"))
            {

                _name = item.prefabName.Substring(0, item.prefabName.Length - 7);
                Debug.Log(_name);
            }



            HiddenObject h = GetObject(_name);

            if (h == null)
            {
                Debug.Log("Objeto " + _name + " não encontrado");
                continue;
            }

            h = Instantiate(h, item.pos, Quaternion.identity) as HiddenObject;

            if (item.scale.magnitude > 0)
                h.transform.localScale = item.scale;

            h.GetComponent<SpriteRenderer>().sortingOrder = item.sortOrder;

            h.SetProperties(item);

            AddHiddenObject(h);

            h.Init();

            if (currentGameMode == GameMode.EDITOR)
                h.CreateUI();
            else
            {
                //se for boss nao aparece
                if (h.properties.isBoss || h.properties.isBogg)
                {
                    h.SetTransparencyTween(0, 0.1f);
                    h.EnableCollision(false);
                }



            }

            //Aguarda 1 frame se estiver no modo jogo
            if (currentGameMode > 0)
            {
                yield return new WaitForEndOfFrame();

                float progress = (float)List_hiddenObjects.Count / (float)data.objectList.Count;

                GameUI.instance.ShowLoading(true, progress);

            }

        }

        #endregion

        //BG
        _bgLoading = true;
        bgRenderer.sprite = GetBG(data.bg);

        while(_bgLoading) //GetBG is asynchronous so we need to wait for it to load background
            yield return new WaitForEndOfFrame();

        currentGameState = currentGameMode == GameMode.EDITOR ? GameState.PAUSE : GameState.LEVEL_INTRO;
        //reseta o timer
        if (currentGameMode == GameMode.GAME || currentGameMode == GameMode.GAME_EASY)
        {
            var statistic = gameLevelStatisticData.GetLevelStatistics(levelID);
            var stars = (statistic != null) ? statistic.starsCount : 0;
            //totalTime = TimeLeft = Mathf.Clamp(settings.f_timeLimit - (stars * 10), 15, settings.f_timeLimit);
            if(currentGameMode == GameMode.GAME)
                totalTime = TimeLeft = Mathf.Clamp(settings.f_timeLimit - (stars * 10), 40, settings.f_timeLimit);
            else totalTime = TimeLeft = settings.f_timeLimitEasyMode;
        }
        else
            totalTime = TimeLeft = settings.f_timeLimit;


        //Lupa
        findsRemain = (currentGameMode == GameMode.GAME || currentGameMode == GameMode.GAME_EASY) ? currentLevelData.finds :
                                                 IniFile.GetInt("Settings", "time_attack_finds", 3);//settings.i_finds;

        GameUI.instance.ShowLoading(false, 1);

        Reset();

        if (currentGameMode == GameMode.GAME || currentGameMode == GameMode.GAME_EASY)
            SetupRegularGame();
        else
            if (currentGameMode == GameMode.GAME_TIMEATTACK)
            SetupTimeAttackGame();


        //inicia o level 
        if (currentGameMode > 0)
            StartCoroutine(OnLevelStart());
        else
            OnLevelLoaded(true);
    }

    /// <summary>
    /// Obtem o objeto da lista estática que deve ser carregada apenas uma vez da Resources
    /// </summary>
    /// <returns>The object.</returns>
    /// <param name="objecName">Objec name.</param>
    static HiddenObject GetObject(string objecName)
    {
        //if(objects == null)
        //	FillObjectsListEditor();

        HiddenObject h = null;

        foreach (var item in objects)
        {

            if (item.name.Equals(objecName))
            {
                return item;
            }
        }

        //Não encontrou, procura na resources

        h = Resources.Load<HiddenObject>("Obj/" + objecName);

        //objects.Add(h);

        return h;
    }

    /// <summary>
    /// Obtem o BG para o level. 
    /// </summary>
    /// <returns>The B.</returns>
    /// <param name="bgName">Background name.</param>
    private Sprite GetBG(string bgName)
    {
        Sprite bg = null;

        foreach (var item in bgs)
        {
            if (item.name.Equals(bgName))
                return item;
        }

        Addressables.LoadAssetAsync<Sprite>(bgName).Completed += OnBackgroundLoaded;//Release done in ReleaseAddressables()
        //bgs.Add(bg);
        return bg;
    }
        
    private void OnBackgroundLoaded(AsyncOperationHandle<Sprite> obj)
    {
        _loadedBackground = obj;
        bgRenderer.sprite = obj.Result;
        _bgLoading = false;
    }
    
    /// <summary>
    /// Configura o time attack
    /// </summary>
    void SetupTimeAttackGame()
    {
        //Loga no GA
        string scn = currentLevelData.levelID.Split('_')[1];
        string log = currentLevelData.character + "_" + scn + "_TimeAttackStarted";

        List<HiddenObject> tempList = new List<HiddenObject>();
        for (int i = 0; i < List_hiddenObjects.Count; i++)
        {

            HiddenObject h = List_hiddenObjects[i];

            if (!h.properties.isBoss && !h.properties.isBogg)
            {
                tempList.Add(h);
                h.SetClickable(true);
                h.CreateUI();
            }
        }
        List_hiddenObjects = tempList;
    }

    /// <summary>
    /// Monta os objetos para o jogo normal
    /// </summary>
    void SetupRegularGame()
    {
        //de todos os objetos, 33% do minimo clicavel devem ser importants (parametrizavel no ini)
        int minObjects = settings.i_minObjects;
        int minImportants = (int)((float)minObjects * settings.f_minImportantObjects / 100);

        //checa quantos objetos importantes existem na lista
        int importantsCount = 0;
        var tempListNonImportant = new List<HiddenObject>();//Lista de não importantes temporaria        
        var tempList = new List<HiddenObject>();//cria lista temporária (final)        
        var tempListImportant = new List<HiddenObject>();//Lista de importantes temporaria        
        var boggsList = new List<HiddenObject>();//Lista de Boggs

        //marca todos os objetos como nao clicaveis
        foreach (var item in List_hiddenObjects)
        {
            item.SetClickable(false);

            if (item.properties.isBoss == false && item.properties.isBogg == false)
            {
                if (item.properties.isImportant)
                {
                    tempListImportant.Add(item);
                    importantsCount++;
                }
                else
                    tempListNonImportant.Add(item);
            }
            else
            {
                if (item.properties.isBogg)
                    boggsList.Add(item);
                else
                    tempList.Add(item);
            }
        }

        //verifica se está dentro do mínimo
        if (minImportants > importantsCount)
            minImportants = importantsCount;

        //Objetos importantes
        for (int i = 0; i < minImportants; i++)
        {
            int rdm = UnityEngine.Random.Range(0, tempListImportant.Count);
            HiddenObject obj = tempListImportant[rdm];

            tempList.Add(obj);
            tempListImportant.Remove(obj);

            minObjects--;
        }

        //objetos nao importantes
        for (int i = 0; i < minObjects; i++)
        {
            int rdm = UnityEngine.Random.Range(0, tempListNonImportant.Count);
            HiddenObject obj = tempListNonImportant[rdm];

            tempList.Add(obj);
            tempListNonImportant.Remove(obj);
        }

        //Escolhe apenas dois Boggs
        if (boggsList.Count > 0)
        {
            levelHasBoggs = true;

            for (int i = 0; i < 2; i++)
            {
                int rdm = UnityEngine.Random.Range(0, boggsList.Count);
                boggsList.RemoveAt(rdm);
            }

            tempList.AddRange(boggsList); //adiciona a lista de objetos

            //elege o bogg 1
            boggs1 = boggsList[0];
            boggs2 = boggsList[1];
        }

        //-------
        List_hiddenObjects = tempList;        
        foreach (var item in List_hiddenObjects)
        {

            if (!item.properties.isBoss && !item.properties.isBogg)
            {
                item.SetClickable(true);
                item.CreateUI();
            }
        }
    }

    /// <summary>
    /// Limpa todos os objetos da tela
    /// </summary>
    public void ClearAll()
    {
        HiddenObject[] objs = FindObjectsOfType<HiddenObject>();

        foreach (var item in objs)
        {
            Destroy(item.gameObject);
        }

        List_hiddenObjects.Clear();

        //Reseta a UI
        GameUI.instance.CanvasGroup_victory.alpha = 0;
    }

    public void ChangeBG(int index)
    {
        if(index < bgs.Count)
            bgRenderer.sprite = bgs[index];
        else Debug.LogError("ChangeBG: Index too high");
    }

    /// <summary>
    /// Usa a lupa
    /// </summary>
    public void UseFind()
    {
        if (currentGameState != GameState.IN_GAME)
            return;

        if (isPaused)
            return;

        if (findsRemain <= 0)
            return;

        if (findCooldown > 0)
            return;

        //evita o jogador de usar a lupa seguidas vezes
        findCooldown = 3;

        SoundManager.PlaySFX("sfx04");
        BlinkObject();

        findsRemain--;
    }

    public bool TogglePause()
    {
        if (currentGameState != GameState.IN_GAME)
            return false;

        isPaused = !isPaused;

        //Debug.Log(isPaused);
        return isPaused;

    }

    public void SetPause(bool pause)
    {
        isPaused = pause;
    }

    /// <summary>
    /// Salva a quantidade de estrelas ganhas no level
    /// </summary>
    public void SaveStatisticsData()
    {
        string serialized = JsonUtility.ToJson(gameLevelStatisticData);
        PlayerPrefs.SetString("levels", serialized);
        PlayerPrefs.Save();
        //Debug.Log(serialized);
    }

    /// <summary>
    /// Carrega a quatidade de estrelas ganhas em cada level
    /// </summary>
    public void LoadLevelStatisticsData()
    {
        if (PlayerPrefs.HasKey("levels"))
            gameLevelStatisticData = JsonUtility.FromJson<GameLevelStisticsData>(PlayerPrefs.GetString("levels"));
    }

    public static T ParseEnum<T>(string value)
    {
        return (T)System.Enum.Parse(typeof(T), value, true);
    }

    public void SetBlurEffect(bool enable)
    {
        blurEffect.enabled = enable;
    }


    public void OnClick(RaycastHit2D[] hit)
    {
        //Verifica se está em modo game
        if ((int)currentGameMode < 1 || currentGameState != GameState.IN_GAME || isPaused)
            return;

        bool found = false;

        if (hit.Length > 0)
        {


            foreach (var item in hit)
            {

                HiddenObject h = item.transform.GetComponent<HiddenObject>();

                if (h == null)
                    continue;

                if (h.isFound)
                    continue;

                if (h.IsValid())
                {
                    h.OnClick();
                    found = true;
                    OnFindObject(h);
                    break;
                }

            }

        }
        else
            found = true; //se não clicou em nada, nem no bg (que tem um collider), não penaliza

        //Clicou e não encontrou objetos, erro...
        if (!found)
            GameManager.instance.OnErrorClick();
    }

    public void RestartLevel()
    {
        ReleaseAddressables();
        Load(currentLevelData.levelID, currentGameMode);
    }

    private void ReleaseAddressables()
    {
        Addressables.Release(_loadedBackground);
        Addressables.Release(_loadedBigChar);
    }

}
