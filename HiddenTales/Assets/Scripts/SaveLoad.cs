﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using PlayerPrefs = PreviewLabs.PlayerPrefs;

[System.Serializable]
public class CharacterDialog{
	public string character = string.Empty;
	public string dialog = string.Empty; //id do diálogo
}

/// <summary>
/// Representa todo o conteúdo da montagem de um level.
/// </summary>
[System.Serializable]
public class GameLevelData{

	public string levelID = string.Empty;
	public List<HiddenObjectProperties> objectList = new List<HiddenObjectProperties>();

	public string levelOnWin = string.Empty;

	public string levelOnLose = string.Empty;

	public string bg = string.Empty;

	public int finds; //numero de lupas

	public string character = "ALISE";

	public CharacterDialog introDialog;

	public CharacterDialog onWinDialog;

	public CharacterDialog onLoseDialog;

	//public GameLevelStatistics statistics; //Estatísticas deste level

}

public class SaveLoad {

	public static void Save(GameLevelData data)
	{
        string tmp = JsonUtility.ToJson(data);

		Debug.Log(tmp);

		PlayerPrefs.SetString(data.levelID, tmp);
		PlayerPrefs.Flush();
		Debug.Log("Saved as " + data.levelID);

	}

	public static GameLevelData Load(string levelID)
	{
		string data = PlayerPrefs.GetString(levelID);

		GameLevelData level = JsonUtility.FromJson<GameLevelData>(data);

		return level;
	}

	public static bool CheckLevel(string levelID)
	{
		return PlayerPrefs.HasKey(levelID);
	}

	public static string[] GetAllLevels()
	{
		Hashtable hash = PlayerPrefs.GetHashTable();

		List<string> list = new List<string>();

		foreach (string item in hash.Keys) {
			list.Add(item);
		}

		string[] str = list.ToArray();

		return str;
	}

	public static void DeleteLevel(string levelID)
	{
		if(PlayerPrefs.HasKey(levelID)){
			PlayerPrefs.DeleteKey(levelID);
			Debug.Log("Level " + levelID + " Deleted!");
			PlayerPrefs.Flush();
		}
	}

}
