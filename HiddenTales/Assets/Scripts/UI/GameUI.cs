﻿using UnityEngine;
//using System.Collections;
using System.Collections.Generic;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;
using UnityEngine.SceneManagement;

public class GameUI : MonoBehaviour {

	public static GameUI instance;

	[Header("Main")]
	public RectTransform rectArray;
	public GameObject arrayItemOriginal; //prefab do hidden object da UI
	public Text timeLimit;
	public Canvas canvas{get; private set;}
	public CanvasGroup CanvasGroup_victory;
	public CanvasGroup CanvasGroup_hud;
	public CanvasGroup CanvasGroup_pauseMenu;
	public CanvasGroup CanvasGroup_loading;
	public CanvasGroup CanvasGroup_victoryTime;
	public Text Text_timeResult;
	public Slider slider_loading;
	public Text victoryText;
	public Text findRemain;
	public Button Button_pause;
	public Sprite playSprite;
	private Sprite pauseSprite;
	public CanvasGroup CanvasGroup_ItemsRemaining;
	public Text Text_itemsRemaining;
	public Text Text_bestTime;
	public Text Text_rank;
	int rank;
	bool receivedRank;


	[Header("Audio")]
	public Slider slider_sfx;
	public Slider slider_bgm;


	[Header("Dialogs")]
	public RectTransform RectTransform_character;
	public RectTransform dialogBackgrd;
	public RectTransform RectTransform_dialog;
	public CanvasGroup skipButton;
	public CanvasGroup playButton;

	[Header("Normal Mode")]
	public CanvasGroup CanvasGroup_NormalModeSwans;
	public CanvasGroup CanvasGroup_victorySwans;
	public RectTransform[] stars; //estrelas da HUD
	public RectTransform[] starsEnding; //estrelas do final do level
	private int _starsCountNormalMode;

	[Header("Time Attack")]
	[SerializeField]
	CanvasGroup CanvasGroup_TimeAttackCanvas;
	[SerializeField]
	CanvasGroup CanvasGroup_TimeAttackSwans;
	public RectTransform[] starsTimeAttack;
	private int _starsCountTimeAttack;

	[Header("Easy Mode")]
	[SerializeField]
	CanvasGroup CanvasGroup_EasyModeCanvas;
	[SerializeField]
	CanvasGroup CanvasGroup_EasyModeSwans;
	public CanvasGroup CanvasGroup_victorySwans_EasyMode;
	public RectTransform[] starsEasyMode;
	public RectTransform[] starsEndingEasyMode; //estrelas do final do level
	private int _starsCountEasyMode;

	private List<HiddenObjectUI> objects = new List<HiddenObjectUI>(); 

	private Blur cameraBlur;

	public bool dialogSkipped {get; private set;}

	void Awake()
	{
		instance = this;

		CanvasGroup_victory.alpha = 0;

		pauseSprite = Button_pause.image.sprite;

		cameraBlur = Camera.main.GetComponent<Blur>();
	}

	void OnEnable()
	{
		GameManager.OnObjectFound += OnObjectFound;
	}

	void OnDisable()
	{
		GameManager.OnObjectFound -= OnObjectFound;
    }

	// Use this for initialization
	void Start () {
		
		canvas = GetComponent<Canvas>();

		//personagem e dialogos fora da area do jogo
		RectTransform_character.anchoredPosition = new Vector2(-300, RectTransform_character.anchoredPosition.y);
		dialogBackgrd.GetComponent<CanvasGroup>().alpha = 0;

		//obtem os volumes
		slider_bgm.value = SoundManager.bgmVolume;
		slider_sfx.value = SoundManager.sfxVolume;

		CanvasGroup_pauseMenu.gameObject.SetActive(true);

		//fecha o menu pause
		TogglePauseMenu(false,0f);
		if (PlayerPrefs.GetString (SceneManager.GetActiveScene().name) == "") {
			PlayerPrefs.SetString (SceneManager.GetActiveScene().name, "Y");
			skipButton.alpha = 0;
			skipButton.interactable = false;
			skipButton.blocksRaycasts = false;
		}
			
		ShowLoading(false, 0);

	}

	// Update is called once per frame
	void Update () {

		float timer = 0;

		if(GameManager.instance == null)
			return;

		//Update volume
		if(GameManager.instance.isPaused)
			SoundManager.SetVolume(slider_sfx.value, slider_bgm.value);

		if(GameManager.instance.settings == null)
			return;

		//if(GameManager.instance.currentGameState != GameState.IN_GAME){
		//	timeLimit.text = "0:00";
		//	return;
		//}

		//timer
		timer = (GameManager.currentGameMode == GameMode.GAME || GameManager.currentGameMode == GameMode.GAME_EASY) ? GameManager.instance.TimeLeft : GameManager.instance.TimeSpan;

		if (GameManager.currentGameMode == GameMode.GAME_TIMEATTACK)
			timeLimit.fontSize = 56;
		else
			timeLimit.fontSize = 81;
		
		//miliSeconds = Mathf.Clamp(miliSeconds, 0, 0.9f);

		if (GameManager.currentGameMode == GameMode.GAME_TIMEATTACK)
		{
			timeLimit.text = GetNiceBestTime(timer);
		}
		else
		{
			timeLimit.text = GetNiceTime(timer);
		}

		//Lupa
		findRemain.text = "x" + GameManager.instance.findsRemain.ToString();


		int gameManagerStarsCount = GameManager.instance.starsCount;
		int gameManagerStarsCountTimeAttack = GameManager.instance.starsCountTimeAttack;
		int gameManagerStarsCountEasyMode = GameManager.instance.starsCountEasyMode;

		//controla as estrelas
		if (_starsCountNormalMode != gameManagerStarsCount) 
		{
			for (int i = 0; i < stars.Length; i++)
			{
				//stars[i].color = gameManagerStarsCount > i ? Color.white : Color.clear;

				if (i > gameManagerStarsCount - 1)
				{
					//stars[i].color = Color.clear;
					LeanTween.scale(stars[i], new Vector3(4,4,1), 1);
					LeanTween.alpha(stars[i], 0, 1);
				}
			}

			_starsCountNormalMode = gameManagerStarsCount;
		}

		if(_starsCountEasyMode != gameManagerStarsCountEasyMode)
		{
			for (int i = 0; i < starsEasyMode.Length; i++)
			{
				//stars[i].color = gameManagerStarsCount > i ? Color.white : Color.clear;

				if (i > gameManagerStarsCountEasyMode - 1)
				{
					//stars[i].color = Color.clear;
					LeanTween.scale(starsEasyMode[i], new Vector3(4, 4, 1), 1);
					LeanTween.alpha(starsEasyMode[i], 0, 1);
				}
			}

			_starsCountEasyMode = gameManagerStarsCountEasyMode;
		}

		if (_starsCountTimeAttack != gameManagerStarsCountTimeAttack)
		{
			for (int i = 0; i < starsTimeAttack.Length; i++)
			{
				//stars[i].color = gameManagerStarsCount > i ? Color.white : Color.clear;

				if (i > gameManagerStarsCountTimeAttack - 1)
				{
					//stars[i].color = Color.clear;
					LeanTween.scale(starsTimeAttack[i], new Vector3(4, 4, 1), 1);
					LeanTween.alpha(starsTimeAttack[i], 0, 1);
				}
			}

			_starsCountTimeAttack = gameManagerStarsCountTimeAttack;
		}

	}

	string GetNiceTime(float time)
	{
		int minutes = Mathf.FloorToInt(time / 60F);
		int seconds = Mathf.FloorToInt(time - minutes * 60);


		float miliSeconds = time - Mathf.FloorToInt(time);

		if (miliSeconds > 0.9f)
			miliSeconds = 0.9f;

		return string.Format("{0:0}:{1:00}", minutes, seconds);
	}

	string GetNiceBestTime(float time)
	{
		int minutes = Mathf.FloorToInt(time / 60F);
		int seconds = Mathf.FloorToInt(time - minutes * 60);


		float miliSeconds = time - Mathf.FloorToInt(time);

		if (miliSeconds > 0.9f)
			miliSeconds = 0.9f;

		return string.Format("{0:0}:{1:00}{2:.0}", minutes, seconds, miliSeconds);

	}

	/// <summary>
	/// Resets the stars hud.
	/// </summary>
	public void ResetStarsHud()
	{
		for (int i = 0; i < stars.Length; i++)
		{
			LeanTween.scale(stars[i], new Vector3(1, 1, 1), 0);
			LeanTween.alpha(stars[i], 1, 0);
		}

		for (int i = 0; i < starsTimeAttack.Length; i++)
		{
			LeanTween.scale(starsTimeAttack[i], new Vector3(1, 1, 1), 0);
			LeanTween.alpha(starsTimeAttack[i], 1, 0);
		}

		for (int i = 0; i < starsEasyMode.Length; i++)
		{
			LeanTween.scale(starsEasyMode[i], new Vector3(1, 1, 1), 0);
			LeanTween.alpha(starsEasyMode[i], 1, 0);
		}

		CanvasGroup_victoryTime.alpha = 0;
		CanvasGroup_victorySwans.alpha = 0;
		CanvasGroup_victorySwans_EasyMode.alpha = 0;
		CanvasGroup_TimeAttackCanvas.alpha = 0;

		receivedRank = false;
		rank = -10;

		LeanTween.alpha(Text_rank.rectTransform, 0, 0f);

		//Verifica o tipo de jogo
		if (GameManager.currentGameMode == GameMode.GAME_TIMEATTACK)
		{
			CanvasGroup_NormalModeSwans.alpha = 0;
			CanvasGroup_EasyModeSwans.alpha = 0;
			CanvasGroup_ItemsRemaining.alpha = 1;

			Text_bestTime.text = GetNiceBestTime(GameManager.instance.BestTime);

			Text_bestTime.GetComponentInParent<CanvasGroup>().alpha = 1;

			Text_itemsRemaining.text = GameManager.instance.GetObjectsRemaining().ToString();
		}
		else
		{
			CanvasGroup_NormalModeSwans.alpha = GameManager.currentGameMode == GameMode.GAME ? 1 : 0;
			CanvasGroup_EasyModeSwans.alpha = GameManager.currentGameMode == GameMode.GAME_EASY ? 1 : 0;
			CanvasGroup_ItemsRemaining.alpha = 0;

			Text_bestTime.GetComponentInParent<CanvasGroup>().alpha = 0;
		}

	}

	public HiddenObjectUI CreateObjUI(HiddenObject obj)
	{
		GameObject go = Instantiate(arrayItemOriginal);

		go.transform.SetParent(rectArray, false);

		HiddenObjectUI ui = go.GetComponent<HiddenObjectUI>();

		ui.Init(obj);

		objects.Add(ui);

		return ui;
	}

	public float OnWin(int starCount, bool boggs, GameMode gameMode)
	{
		
		Vector3 originalScale = CanvasGroup_victory.GetComponent<RectTransform>().localScale;

		for (int i = 0; i < starsEnding.Length; i++) {
			LeanTween.alpha(starsEnding[i], 0, 0);
		}

		for (int i = 0; i < starsEndingEasyMode.Length; i++)
		{
			LeanTween.alpha(starsEndingEasyMode[i], 0, 0);
		}

		//victoryText.text = starCount > 2 ? "AMAZING!" : starCount > 1 ? "GOOD!" : starCount > 0 ? "THAT WAS \nCLOSE!" : "TIME's UP!";
		if (gameMode == GameMode.GAME)
		{
			victoryText.text = boggs ? Dialogs.GetDialog("end_boggs").text : Dialogs.GetDialog("end" + starCount.ToString()).text;
			CanvasGroup_victorySwans.alpha = 1;
		}
		if (gameMode == GameMode.GAME_EASY)
		{
			string noBoggsVariantText = starCount > 0 ? Dialogs.GetDialog("end3").text : Dialogs.GetDialog("end0").text;
			victoryText.text = boggs ? Dialogs.GetDialog("end_boggs").text : noBoggsVariantText;
			CanvasGroup_victorySwans_EasyMode.alpha = 1;
		}
		LeanTween.scale(victoryText.gameObject, Vector3.one * 1.2f, 1).setEase(LeanTweenType.easeInElastic).setLoopPingPong(1);

		//canvas aparece
		LeanTween.alphaCanvas(CanvasGroup_victory, 1, 1).setOnComplete(
			() =>
			{

				//estrelas aparecemm
				if (gameMode == GameMode.GAME)
					ShowVictorySwansNormalMode(starCount);
				if (gameMode == GameMode.GAME_EASY)
					ShowVictorySwansEasyMode(starCount);

				//canvas desaparece
				LeanTween.alphaCanvas(GameUI.instance.CanvasGroup_victory, 0, 1).setDelay(starCount + 2);


			});
//		LeanTween.scale(victoryCanvasGroup.GetComponent<RectTransform>(), originalScale*8.9f, 1).setOnComplete(
//			()=>{
//				LeanTween.scale(victoryCanvasGroup.GetComponent<RectTransform>(), originalScale, 1).setDelay(1);
//				LeanTween.alphaCanvas(GameUI.instance.victoryCanvasGroup,0,1).setDelay(1);
//			});


		//limpa a lista de UIs
		objects.Clear();

		return 4+starCount;
	
	}

	private void ShowVictorySwansNormalMode(int starCount)
    {
		for (int i = 0; i < starsEnding.Length; i++)
		{
			if (starCount > i)
			{
				//LeanTween.rotate(starsEnding[i], -360, 1).setDelay(i).setEase(LeanTweenType.easeInOutSine);
				RectTransform go = starsEnding[i];
				LeanTween.alpha(starsEnding[i], 1, 1).setDelay(i).setOnComplete(() =>
				{
					//Inicializa o animator
					go.GetComponent<Animator>().SetTrigger("onTrigger");
				});
			}
		}
	}

	private void ShowVictorySwansEasyMode(int starCount)
	{
		for (int i = 0; i < starsEndingEasyMode.Length; i++)
		{
			if (starCount > i)
			{
				//LeanTween.rotate(starsEnding[i], -360, 1).setDelay(i).setEase(LeanTweenType.easeInOutSine);
				RectTransform go = starsEndingEasyMode[i];
				LeanTween.alpha(starsEndingEasyMode[i], 1, 1).setDelay(i).setOnComplete(() =>
				{
					//Inicializa o animator
					go.GetComponent<Animator>().SetTrigger("onTrigger");
				});
			}
		}
	}
	/// <summary>
	/// Ons the time attack end.
	/// </summary>
	public void OnTimeAttackEnd()
	{
		Vector3 originalScale = CanvasGroup_victory.GetComponent<RectTransform>().localScale;


		victoryText.text = Dialogs.GetDialog("message_timeattack").text;
		LeanTween.scale(victoryText.gameObject, Vector3.one * 1.2f, 1).setEase(LeanTweenType.easeInElastic).setLoopPingPong(1);

		Text_timeResult.text = timeLimit.text;

		//canvas aparece
		LeanTween.alphaCanvas(CanvasGroup_victory, 1, 1).setOnComplete(
			() =>
			{

				LeanTween.alphaCanvas(CanvasGroup_victoryTime, 1, 1);


				//canvas desaparece
				//LeanTween.alphaCanvas(CanvasGroup_victoryTime, 0, 1).setDelay(2);


			});

	}

	public void ShowNewRecord()
	{
		
		LeanTween.alpha(victoryText.rectTransform, 0, 0.3f).setOnComplete(
			()=>victoryText.text = Dialogs.GetDialog("message_newrecord").text);

		LeanTween.alpha(victoryText.rectTransform, 1, 0.3f).setDelay(0.3f);

		//hud piscaa
		CanvasGroup cg = Text_bestTime.GetComponentInParent<CanvasGroup>();
		//LeanTween.alphaCanvas(cg, 0, 0).setDelay(0.2f).setOnComplete(() => LeanTween.alphaCanvas(cg, 1, 0).setDelay(0.2f)).setLoopPingPong();

		LeanTween.alphaCanvas(CanvasGroup_victoryTime, 0, 0.3f).setLoopPingPong(5);

	}

	public void ShowTimeAttackEnabled(bool enable)
	{
		LeanTween.alphaCanvas(CanvasGroup_TimeAttackCanvas, enable ? 1 : 0, 1);
	}


	public void OnTimeAttackStart()
	{
		Vector3 originalScale = CanvasGroup_victory.GetComponent<RectTransform>().localScale;

		CanvasGroup_victorySwans.alpha = 0;
		CanvasGroup_victorySwans_EasyMode.alpha = 0;
		CanvasGroup_TimeAttackSwans.alpha = 1;
		CanvasGroup_victoryTime.alpha = 0;

		victoryText.text = Dialogs.GetDialog("timeattack").text + "!";
		//LeanTween.scale(victoryText.gameObject, Vector3.one * 1.2f, 1).setEase(LeanTweenType.easeInElastic).setLoopPingPong(1);

		//Text_timeResult.text = timeLimit.text;

		//canvas aparec
		LeanTween.alphaCanvas(CanvasGroup_victory, 1, 1).setOnComplete(
			() =>
			{
				//canvas desaparec
				LeanTween.alphaCanvas(CanvasGroup_victory, 0, 1).setDelay(2);


			});

	}


	private void OnObjectFound()
	{
		Debug.Log(GameManager.instance.GetObjectsRemaining());
		Text_itemsRemaining.text = GameManager.instance.GetObjectsRemaining().ToString();
	}


	public bool ShowDialogs(CharacterDialog dialog, Sprite characterSprite)
	{
		RectTransform_dialog.gameObject.SetActive(true);
		Debug.Log("<Color=Yellow>Show Dialogs</Color>");
		//Configura o sprite do personagem

        RectTransform_character.GetComponent<Image>().sprite = characterSprite;

		string text = string.Empty;
		DialogData data = null;
		if(dialog.dialog.Trim() != string.Empty){
			data = Dialogs.GetDialog (dialog.dialog);//Application.systemLanguage.ToString());
			Debug.Log("Getting dialog: "+ dialog.dialog);
		}
		else
			return false;

		if(data != null)
			text = data.text;
		Debug.Log (text);
		if(text == string.Empty)
			text = "There is no dialog defined... id:" + dialog.dialog;

		//altera texto
		dialogBackgrd.GetComponentInChildren<Text>().text = text;

		RectTransform_character.anchoredPosition = new Vector2(-300,0);

		//Personagem aparece
		SoundManager.PlaySFX("sfx08");
		LeanTween.move(RectTransform_character, new Vector2(682, 0), 1).setEase(LeanTweenType.easeOutElastic);
		//Caixa de diálogo aparece
		LeanTween.alphaCanvas(dialogBackgrd.GetComponent<CanvasGroup>(), 1, 1).setDelay(1).setOnComplete(
			()=>{
				if(data != null){
					if (PlayerPrefs.GetString (data.audioFile) == "") {
						PlayerPrefs.SetString (data.audioFile, "Y");

					}
					SoundManager.PlayDialog(data.audioFile);
					Debug.Log("Audio file: "+data.audioFile);
				}

				skipButton.interactable = true;
				skipButton.blocksRaycasts = true;
				LeanTween.alphaCanvas(skipButton, 1, 1);
			}
		);


		return true;
	}
	
    public void HideDialogs(bool immediately = false)
	{
		Debug.Log ("Hide Dialogs");
		if(immediately)
		{
			LeanTween.cancelAll();
			LeanTween.move(RectTransform_character, new Vector2(2400, 0), 0.1f);
			LeanTween.alphaCanvas(dialogBackgrd.GetComponent<CanvasGroup>(), 0, 0.1f);
			LeanTween.alphaCanvas(skipButton, 0, 0.1f);
			skipButton.interactable = false;
			skipButton.blocksRaycasts = false;
			return;
		}

        LeanTween.move(RectTransform_character, new Vector2(2400, 0), 0.3f)
            .setEase(LeanTweenType.easeInSine).setDelay(1)
            .setOnStart(() => { SoundManager.PlaySFX("sfx09"); });

		LeanTween.alphaCanvas(dialogBackgrd.GetComponent<CanvasGroup>(), 0, 1);
		LeanTween.alphaCanvas(skipButton, 0, 1f);
		skipButton.interactable = false;
		skipButton.blocksRaycasts = false;
		dialogSkipped = false;
	}

	/// <summary>
	/// Remove o objeto da lista de objetos e HUD
	/// </summary>
	/// <param name="obj">Object.</param>
	public void RemoveObject(HiddenObjectUI obj)
	{
		
		if(objects.Contains(obj) == false)
			return;

		objects.Remove(obj);

		if(obj != null)
			Destroy(obj.gameObject);
	}

	/// <summary>
	/// Usa a lupa
	/// </summary>
	public void UseFind()
	{
		if(GameManager.instance == null)
			return;
		GameManager.instance.UseFind();
	}

	/// <summary>
	/// Botao Skip da UI
	/// </summary>
	public void SkipButton()
	{
		
		SoundManager.instance.dialogsAudioSource.Stop ();
		dialogSkipped = true;
	}

	public void StartButton()
	{
		ShowStartButton(false);
		StartCoroutine(GameManager.instance.StartGame());
    }

	public void ShowStartButton(bool makeVisible)
    {
		playButton.interactable = makeVisible;
		playButton.alpha = makeVisible ? 1 : 0;
		playButton.blocksRaycasts = makeVisible;
	}

	/// <summary>
	/// Botao pause da UI
	/// </summary>
	public void Pause()
	{
		
		bool isPaused = GameManager.instance.TogglePause();

		//troca o sprite
		Button_pause.image.sprite = isPaused ? playSprite : pauseSprite;

		cameraBlur.enabled = isPaused;

		TogglePauseMenu(isPaused, 0.1f);

		//Salva no playerprefs
		if(!isPaused)
			SoundManager.SaveVolume();
			
	}

	public void ShowHud(bool show, float time)
	{
		LeanTween.alphaCanvas(CanvasGroup_hud, show ? 1 : 0, time);
	}


	/// <summary>
	/// Faz o menu pause aparecer / desaparecer
	/// </summary>
	/// <param name="open">Define se o menu deve aparecer ou desaparecer <c>true</c> open.</param>
	/// <param name="time">Tempo do fade</param>
	public void TogglePauseMenu(bool open, float time)
	{
		
		if(time <= 0){
			CanvasGroup_pauseMenu.alpha = open ? 1 : 0;
			CanvasGroup_pauseMenu.blocksRaycasts = open;
			CanvasGroup_pauseMenu.interactable = open;
			return;
		}

		LeanTween.alphaCanvas(CanvasGroup_pauseMenu, open ? 1 : 0, time).setOnComplete(
			()=>{
				CanvasGroup_pauseMenu.blocksRaycasts = open;
				CanvasGroup_pauseMenu.interactable = open;
			});
		
	}

	public void RestartLevel()
	{
		
		SoundManager.SaveVolume();

		SoundManager.StopBGM();

		ShowHud(false,0.3f);
		TogglePauseMenu(false, 0f);

		Fade.FadeOut(1.5f, 100, 
			()=>{
				Button_pause.image.sprite = pauseSprite;
				GameManager.instance.RestartLevel();
			});



	}

	public void ExitToMenu()
	{

		ShowHud(false,0.3f);
		TogglePauseMenu(false, 0f);

		Fade.FadeOut(1.5f, 100, 
			()=>{
				Button_pause.image.sprite = pauseSprite;
				UnityEngine.SceneManagement.SceneManager.LoadScene("LevelSelect");
			});
		
		SoundManager.SaveVolume();

	}

	public void ShowLoading(bool show, float progress)
	{
		
		if (show)
		{
			CanvasGroup_loading.alpha = 1;
			slider_loading.value = progress;
		}
		else
			CanvasGroup_loading.alpha = 0;
	}

}
