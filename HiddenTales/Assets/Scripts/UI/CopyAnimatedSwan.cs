﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CopyAnimatedSwan : MonoBehaviour {

	Image image;

	void Awake () {

		image = GetComponent<Image>();
		
	}

	void Start()
	{
		ChapterSelect.instance.List_imagesToCopy.Add(image);
	}

	
	// Update is called once per frame
	//void Update () {
		//image.sprite = ChapterSelect.instance.image_animatedSwan.sprite;
	//}
}
