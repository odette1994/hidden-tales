﻿using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;

public class AddressableSpriteLoader : MonoBehaviour
{
    [SerializeField] private AssetReference _spriteAddressable;

    private Image _myImage;
    private AsyncOperationHandle<Sprite> _loadedObj;

    void Awake()
    {
        _myImage = GetComponent<Image>();
    }

    void OnEnable()
    {
        _spriteAddressable.LoadAssetAsync<Sprite>().Completed += SpriteLoaded;
    }

    private void SpriteLoaded(AsyncOperationHandle<Sprite> obj)
    {
        _loadedObj = obj;
        _myImage.sprite = obj.Result;
    }

    void OnDisable()
    {
        Addressables.Release(_loadedObj);
    }
}
