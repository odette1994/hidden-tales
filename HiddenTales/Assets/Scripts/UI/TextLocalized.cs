﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Se inserido um Text da UI, obtem automaticamente o texto pelo ID
/// </summary>
public class TextLocalized : MonoBehaviour
{
    public string dialogID;

    public bool toUpper;

    public string textAfter; //algum texto depois do texto encontrado?

    private Text text;

    // Use this for initialization
    void Awake()
    {
        text = GetComponent<Text>();
        Dialogs.OnLanguageChanged += OnChangeLanguage;
    }

    void Start()
    {
        UpdateText();
    }

    void OnChangeLanguage(Languages newLanguage)
    {
        UpdateText();
    }

    private void UpdateText()
    {
        if (dialogID != string.Empty)
        {
            text.text = Dialogs.GetDialog(dialogID).text + textAfter;
            if (toUpper)
                text.text = text.text.ToUpper();
        }
    }

    void OnDestroy()
    {
        Dialogs.OnLanguageChanged -= OnChangeLanguage;
    }
}
