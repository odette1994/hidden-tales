﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashLightBoss : MonoBehaviour
{
    public GameObject FlashLightContainer;

    public GameObject centerObj;
    public GameObject topObj;
    public GameObject downObj;
    public GameObject leftObj;
    public GameObject rightObj;

    SpriteRenderer center;
    SpriteRenderer top;
    SpriteRenderer down;
    SpriteRenderer left;
    SpriteRenderer right;

    public Transform startPosition;
    public Transform[] AllPosition;
    public float[] MovePositionTime;
    int positionIndex = 0;

    public float fadeInTime = 0.4f;
    public float fadeOutTime = 0.3f;
    public float firstMoveTime = 0.6f;
    public float firstMoveTimeDelay = 0.8f;

    bool IsFristMove = true;
    bool IsFristDelay = false;




    // Start is called before the first frame update
    void Start()
    {
        
        center = centerObj.GetComponent<SpriteRenderer>();
        top = topObj.GetComponent<SpriteRenderer>();
        down = downObj.GetComponent<SpriteRenderer>();
        left = leftObj.GetComponent<SpriteRenderer>();
        right = rightObj.GetComponent<SpriteRenderer>();

        ResetObj();
    }

    void ResetObj()
    {
        Hide();
        positionIndex = 0;
        IsFristMove = true;
        IsFristDelay = false;
        FlashLightContainer.transform.position = new Vector3(startPosition.position.x, startPosition.position.y, startPosition.position.z);
    }

    void Show()
    {
        ShowRender(center);
        ShowRender(top);
        ShowRender(down);
        ShowRender(left);
        ShowRender(right);
    }

    void Hide()
    {
        HideRender(center);
        HideRender(top);
        HideRender(down);
        HideRender(left);
        HideRender(right);
    }

    void ShowRender(SpriteRenderer sp)
    {
        sp.color = new Color(1f, 1f, 1f, 1f);
    }

    void HideRender(SpriteRenderer sp)
    {
        sp.color = new Color(1f, 1f, 1f, 0f);
    }

    private void OnCompleteFadeIn()
    {       
        MoveFlashLight();
    }

    private void MoveFlashLight()
    {
        float moveTime = IsFristMove ? firstMoveTime : MovePositionTime[positionIndex];        
        Vector3 v3 = new Vector3(AllPosition[positionIndex].position.x, AllPosition[positionIndex].position.y, AllPosition[positionIndex].position.z);
        positionIndex = positionIndex + 1 < AllPosition.Length ? positionIndex + 1 : 0;
        if (IsFristDelay)
        {
            LeanTween.move(FlashLightContainer, v3, moveTime).setOnComplete(MoveFlashLight).setDelay(firstMoveTimeDelay);
        }
        else
        {
            if (IsFristMove)
                LeanTween.move(FlashLightContainer, v3, moveTime).setOnComplete(MoveFlashLight).setEase(LeanTweenType.easeOutBack);
            else
                LeanTween.move(FlashLightContainer, v3, moveTime).setOnComplete(MoveFlashLight).setEase(LeanTweenType.linear);
        }

        IsFristDelay = IsFristMove;
        IsFristMove = false;
    }

    public void StartFlashLight()
    {
        ResetObj();
        LeanTween.alpha(centerObj, 1f, fadeInTime);
        LeanTween.alpha(topObj, 1f, fadeInTime);
        LeanTween.alpha(downObj, 1f, fadeInTime);
        LeanTween.alpha(leftObj, 1f, fadeInTime);
        LeanTween.alpha(rightObj, 1f, fadeInTime).setOnComplete(OnCompleteFadeIn);

    }

    public void StopFlashLight()
    {
        LeanTween.cancel(FlashLightContainer);
        LeanTween.alpha(centerObj, 0f, fadeOutTime);
        LeanTween.alpha(topObj, 0f, fadeOutTime);
        LeanTween.alpha(downObj, 0f, fadeOutTime);
        LeanTween.alpha(leftObj, 0f, fadeOutTime);
        LeanTween.alpha(rightObj, 0f, fadeOutTime);
    }

    private void OnDisable()
    {
        StopFlashLight();
    }

}
