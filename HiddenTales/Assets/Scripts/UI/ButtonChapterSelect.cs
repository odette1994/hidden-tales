﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ButtonChapterSelect : MonoBehaviour
{
    [Tooltip("Must use same name of buy button for DLC chapter")]//if purchase chapter, the name must be copy of product name.
    public string chapterStorageName;
    public Characters character; //qual é o personagem deste botao
    public Sprite lockedButton;
    public int minimumStars; //numero minimo de estrelas necessarias para habilitar este level
    public string firstLevel; //primeiro level deste botao
    public GameObject particleUI; //Particula da animação de 15 estrelas
    public GameObject SwanOnImage;

    [HideInInspector]
    public bool isDLC;//Set up at ChapterSelect.cs

    ChapterSelect chapterSelect;
    RectTransform panelStars;
    Button button;
    Text text_remainingStars;
    internal bool isLocked { get; private set; }

    private Sprite originalSprite;

    // Use this for initialization
    void Awake()
    {
        button = GetComponent<Button>();
        chapterSelect = transform.root.GetComponent<ChapterSelect>();
        panelStars = transform.Find("Panel_Stars").GetComponent<RectTransform>();
        text_remainingStars = transform.Find("Text").GetComponent<Text>();
        button.onClick.AddListener(OnClick);
        originalSprite = button.image.sprite;

        

    }

    internal void SetEnable(bool enable)
    {
        if (enable)
        {
            isLocked = false;
            text_remainingStars.enabled = false;
            button.image.sprite = originalSprite;

            SwanOnImage.SetActive(false);
            if (PlayerPrefs.GetInt("complete_charactor_" + Utils.GetCharaterID(character.ToString()), 0) > 0)
            {
                SwanOnImage.SetActive(true);
            }
        }
        else
        {
            isLocked = true;
            button.image.sprite = lockedButton;
            text_remainingStars.text = minimumStars.ToString();
        }
    }

    internal void SetEnableDLC(bool enable)
    {
        var image = GetComponent<Image>();
        var mat = Instantiate(image.material) as Material;
        image.material = mat;

        if (enable)
        {
            isLocked = false;
            text_remainingStars.enabled = false;
            mat.SetFloat("_EffectAmount", 0);
        }
        else
        {
            isLocked = true;
            mat.SetFloat("_EffectAmount", 1);
            text_remainingStars.text = minimumStars.ToString();
        }
    }

    public void OnClick()
    {
        if (ChapterSelect.instance.isCanClickButtonChapter )//Prevent hot fire on button
        {
            ChapterSelect.instance.isCanClickButtonChapter = false;
            StartCoroutine(SetWaitEnableButton());

            //verifica se está mostrando o chapter complete de um pesonagem
            if (ChapterSelect.WillShowCharacterEnding)
                return;

            //Verifica se é possível entrar no capítulo
            if (isLocked)
            {
                //if (InAppHandler.isCanClickPurchase)// Prevent Buy IAP same time with swan unlock
                //{
                    Debug.Log("ChapterSelect.instance.GetSecretSwan():" + ChapterSelect.instance.GetSecretSwan());
                    Debug.Log("minimumStars:" + minimumStars);
                    if (ChapterSelect.instance.GetSecretSwan() >= minimumStars)
                    {
                        PlayerPrefs.SetInt(chapterStorageName + "_unlocked_by_swan", minimumStars);
                        ShowButtonParticle();
                        ChapterSelect.instance.CalculateStars();
                    }
                    if (!LeanTween.isTweening(text_remainingStars.gameObject))
                    {
                        Vector3 scale = text_remainingStars.rectTransform.localScale;
                        LeanTween.scale(text_remainingStars.rectTransform, scale * 2, .8f).setLoopPingPong(1).setEase(LeanTweenType.easeOutElastic);
                    }
               // }


            }
            else
            {
                //Start:FOR test reset to lock
                //PlayerPrefs.SetInt(chapterStorageName + "_unlocked_by_swan", 0);
                //if (isDLC) PlayerPrefs.SetInt(chapterStorageName, 0);
                //END::FOR test reset to lock

                chapterSelect.EnterStageSelection(character, firstLevel);
            }
        }
    }

    IEnumerator SetWaitEnableButton()
    {
        yield return new WaitForSeconds(1);
        ChapterSelect.instance.isCanClickButtonChapter = true;
    }

    public void ShowButtonParticle()
    {
        RectTransform rt = gameObject.GetComponent<RectTransform>();
        Canvas cv = rt.root.GetComponent<Canvas>();
        Vector2 v2 = rt.anchoredPosition;
        RectTransform rootRtLevel1 = gameObject.transform.parent.GetComponent<RectTransform>();
        RectTransform rootRtLevel2 = gameObject.transform.parent.parent.GetComponent<RectTransform>();
        double scaledScreenW = Screen.width / cv.scaleFactor;
        double scaledScreenH = Screen.height / cv.scaleFactor;
        double addX = (scaledScreenW * .5f) - (rootRtLevel1.rect.width * .5f) + rootRtLevel1.anchoredPosition.x + rootRtLevel2.anchoredPosition.x;
        double addY = (scaledScreenH * .5f) - (rootRtLevel1.rect.height * .5f) + rootRtLevel1.anchoredPosition.y + rootRtLevel2.anchoredPosition.y;
        double vxD = (v2.x + addX) * cv.scaleFactor;
        double vyD = (v2.y + addY + rt.rect.height) * cv.scaleFactor;
        ChapterSelect.instance.PlayUnlockParticleAtPoint(Camera.main.ScreenToWorldPoint(new Vector3(float.Parse(vxD.ToString()), float.Parse(vyD.ToString()), 0)));
    }

}
