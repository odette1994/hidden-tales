﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ConfirmationWindow : MonoBehaviour {

	public static System.Action OnWindowRised;
	public static System.Action OnWindowClosed;

	public static ConfirmationWindow instance;

	public delegate void OnYes();

	private static OnYes storedCallback;

	public Text windowTitle;

	private CanvasGroup canvasGroup;

	// Use this for initialization
	void Awake () {
		instance = this;
		canvasGroup = GetComponent<CanvasGroup>();
		windowTitle = GetComponentInChildren<Text>();
		EnableInteract(false);
	}

	public void Yes()
	{
		storedCallback();
		EnableInteract(false);
	}

	public void No()
	{
		EnableInteract(false);
	}

	void OnDestroy()
	{
		instance = null;
	}

	public void RiseWindow(string title, params OnYes[] callback)
	{
		windowTitle.text = title;
		storedCallback = callback[0];
		EnableInteract(true);

		if(OnWindowRised != null)
			OnWindowRised();
	}

	private void EnableInteract(bool enable)
	{
		canvasGroup.alpha = enable ? 1 : 0;
		canvasGroup.interactable = enable;
		canvasGroup.blocksRaycasts = enable;

		if(enable == false)
		if(OnWindowClosed != null)
			OnWindowClosed();
	}

}
