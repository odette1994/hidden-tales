using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulseTween : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        LeanTween.scale(gameObject, new Vector3(1.2f,1.2f), 1).setLoopPingPong();
    }
}
