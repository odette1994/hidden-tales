﻿using UnityEngine;
using UnityEngine.UI;

public class ButtonStageSelect : MonoBehaviour
{
    public string stageIndex = "01";

    public GameLevelStatistics gameLevelStatistic;
    public GameLevelData gameLevelData;
    public string stage;
    public Sprite sprite_btnLocked;
    public Button button_hint;
    public Button button_timeAttack;
    public GameObject[] showStarCollects;

    Image image_buttonBg;
    Button button_stageIcon;

    //Time attack
    Text Text_timeattackBest;
    Text Text_timeAttackTitle;
    Image Image_trophyIcon;

    bool timeAttackEnabled;

    // Use this for initialization
    void Awake()
    {
        showStarCollects[0].SetActive(false);
        showStarCollects[1].SetActive(false);
        showStarCollects[2].SetActive(false);
        button_stageIcon = GetComponent<Button>();
        button_stageIcon.onClick.AddListener(OnClick);
        button_hint.onClick.AddListener(OnClickHint);
        button_timeAttack.onClick.AddListener(OnClickTimeAttack);
        image_buttonBg = GetComponent<Image>();
        Text_timeattackBest = button_timeAttack.transform.Find("Text_Best").GetComponent<Text>();
        Text_timeAttackTitle = button_timeAttack.transform.Find("Text_Title").GetComponent<Text>();
        Image_trophyIcon = button_timeAttack.transform.Find("Image").GetComponent<Image>();
        ChapterSelect.OnEnterLevelSelection += OnEnterLevelSelection;
    }

    void OnDestroy()
    {
        ChapterSelect.OnEnterLevelSelection -= OnEnterLevelSelection;
    }

    void OnEnterLevelSelection()
    {
        //Verifica qual é o estágio referente a este botão
        stage = ChapterSelect.instance.firstLevel.Split('_')[0];
        stage += "_sc" + stageIndex;

        //obtem o nome completo do estagio
        stage = ChapterSelect.instance.GetFullLevelName(stage);

        gameLevelData = SaveLoad.Load(stage);

        //Obtem o level data
        if (ChapterSelect.instance.levelStatistics != null)
            gameLevelStatistic = ChapterSelect.instance.levelStatistics.GetLevelStatistics(stage);

        int starsCount = gameLevelStatistic.starsCount;

        //Verifica se tem estrelas suficientes para o time attack
        timeAttackEnabled = starsCount >= GameManager.minStarsTimeAttack;

        //Configura as infos de besttime
        button_hint.gameObject.SetActive(!timeAttackEnabled);
        button_timeAttack.gameObject.SetActive(timeAttackEnabled);

        Debug.Log("########### Stage ###########");
        Debug.Log(stage);
        Debug.Log("Best Star:"+ gameLevelStatistic.bestStar);


        showStarCollects[0].SetActive(false);
        showStarCollects[1].SetActive(false);
        showStarCollects[2].SetActive(false);
        if (gameLevelStatistic.bestStar > 0)
            showStarCollects[0].SetActive(true);
        if (gameLevelStatistic.bestStar > 1)
            showStarCollects[1].SetActive(true);
        if (gameLevelStatistic.bestStar > 2)
            showStarCollects[2].SetActive(true);

        UpdateBestTime();
        UpdateBoxBG();
    }

    void UpdateBestTime()
    {
        if (!timeAttackEnabled)
            return;

        float timer = gameLevelStatistic.bestTime;

        Image_trophyIcon.gameObject.SetActive(timer > 0);
        Text_timeAttackTitle.gameObject.SetActive(timer <= 0);
        Text_timeattackBest.gameObject.SetActive(timer > 0);

        if (timer <= 0)
            return;

        int minutes = Mathf.FloorToInt(timer / 60F);
        int seconds = Mathf.FloorToInt(timer - minutes * 60);
        float miliSeconds = timer - Mathf.FloorToInt(timer);

        if (miliSeconds > 0.9f)
            miliSeconds = 0.9f;

        var niceTime = string.Format("{0:0}:{1:00}{2:.0}", minutes, seconds, miliSeconds);

        Text_timeattackBest.text = niceTime;
    }

    public void UpdateBoxBG()
    {
        Sprite newSprite = null;
        if (gameLevelStatistic.levelResult != LevelResult.NONE)
        {
            newSprite = ChapterSelect.instance.GetSpriteBox(gameLevelData.bg.Split('_')[1]);
        }

        if (newSprite == null) //That covers levelResult == LevelResult.NONE and GetSpriteBox may return null if desired sprite has not been loaded yet.
            image_buttonBg.sprite = sprite_btnLocked;
        else image_buttonBg.sprite = newSprite;
    }

    void OnClick()
    {
        //Entra no primeiro level
        //ChapterSelect.instance.PlayChapter();
    }

    void OnClickHint()
    {
        //Mostra o Hint
        string message = Dialogs.GetDialog("hint" + stageIndex).text;
        string characterName = gameLevelData.character;

        //Especial para o Rogers
        if (characterName.Contains("ROGERS") && stageIndex != "01")
        {
            message = Dialogs.GetDialog("hint_rogers").text;
        } else if ( ( stageIndex == "03" || stageIndex == "04" || stageIndex == "05"  )  &&  (characterName.Equals("ALISE_AND_LUCAS") || characterName.Equals("PUFFIN") || characterName.Equals("ROTHBART") || characterName.Equals("SCULLY") || characterName.Equals("SPEED")    )
        ){
            if (stageIndex == "03"){
                message = Dialogs.GetDialog("hint06").text;
            }
            else if (stageIndex == "04"){
                message = Dialogs.GetDialog("hint07").text;
            }
            else if (stageIndex == "05"){
                message = Dialogs.GetDialog("hint08").text;
            }

        }

        ChapterSelect.instance.OpenHint(CheckWhatToDo());
    }

    void OnClickTimeAttack()
    {
        ChapterSelect.instance.EnterLevel(stage, GameMode.GAME_TIMEATTACK);
    }

    string CheckWhatToDo()
    {
        string stageToWinOrLose = string.Empty;
        string result = Dialogs.GetDialog("to_play_level").text;
        int count = 0;

        //verifica se esse level está em outros botoes
        foreach (var item in ChapterSelect.instance.ButtonStageSelect_list)
        {
            if (item.gameLevelData.levelOnLose == stage)
            {
                if (count > 0)
                    result += " " + Dialogs.GetDialog("or").text + " ";
                result += " " + Dialogs.GetDialog("lose").text;
                result += " " + Dialogs.GetDialog("level").text + " " + int.Parse(item.stageIndex).ToString();
                count++;
            }

            if (item.gameLevelData.levelOnWin == stage)
            {
                if (count > 0)
                    result += " " + Dialogs.GetDialog("or").text + " ";

                result += " " + Dialogs.GetDialog("win").text;
                result += " " + Dialogs.GetDialog("level").text + " " + int.Parse(item.stageIndex).ToString();
                count++;
            }
        }

        if (count <= 0)
            result += " " + Dialogs.GetDialog("start_chapter").text;

        return result;
    }
}
