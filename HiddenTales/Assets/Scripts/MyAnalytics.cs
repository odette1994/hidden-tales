﻿using Firebase;
using Firebase.Analytics;
using Gamelogic.Extensions;
using System.Threading.Tasks;
using UnityEngine;

public class MyAnalytics : Singleton<MyAnalytics>
{
	private bool _firebaseReady = false;
	private Firebase.FirebaseApp app;

	void Awake()
	{
		if (Instance != this) Destroy(gameObject);
		else DontDestroyOnLoad(gameObject);
	}

	private void Start()
	{
		Debug.Log("[Firebase] AnalyticsWrapper.Start");
		PrepareFirebase();
	}

	private void PrepareFirebase()
    {
		Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
			var dependencyStatus = task.Result;
			if (dependencyStatus == Firebase.DependencyStatus.Available)
			{
				app = Firebase.FirebaseApp.DefaultInstance;

				_firebaseReady = true;
			}
			else
			{
				UnityEngine.Debug.LogError(System.String.Format(
				  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
				// Firebase Unity SDK is not safe to use here.
			}
		});
	}



	public void LogEvent(string eventCategory, string eventAction, string eventLabel, long value)
	{
        Debug.Log("Log Event: " + eventCategory + "/" + eventAction + "/" + eventLabel + "/" + value);
#if !UNITY_EDITOR
		FirebaseAnalytics.LogEvent($"{eventCategory}_{eventAction}", eventLabel, value);
#endif
	}
}