﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Text;

[System.Serializable]
public class DialogData
{
    public string dialogID;
    public string language;
    public string text;
    public string audioFile;
}

public class Dialogs
{
    public static System.Action<Languages> OnLanguageChanged;
    private static string path = Application.persistentDataPath + "/dialogs.txt";
    public static List<DialogData> dialogData = new List<DialogData>();

    /// <summary>
    /// Obtem o diálogo através de um id
    /// </summary>
    /// <returns>The dialog.</returns>
    /// <param name="id">Identifier.</param>
    /// <param name="language">Language.</param>
    public static DialogData GetDialog(string id)
    {
        string language = GetCurrentLanguage().ToString();
        //Debug.Log ("My Language: " + language);

        if (dialogData.Count <= 0)
            Read();

        foreach (var item in dialogData)
        {

            if (item.dialogID.Equals(id) && item.language.Equals(language))
            {
                return item;
            }

        }

        return null;
    }

    /// <summary>
    /// Força a troca de linguagem e grava no playerprefs
    /// </summary>
    /// <param name="language">Language.</param>
    public static void SetLanguage(Languages language)
    {
        PlayerPrefs.SetString("language", language.ToString());
        PlayerPrefs.Save();
        Debug.Log("Changing language to "+language);
        if (OnLanguageChanged != null)
            OnLanguageChanged(language);
    }

    public static Languages GetCurrentLanguage()
    {
        string lang = PlayerPrefs.GetString("language");

        if (string.IsNullOrEmpty(lang))
            return Languages.English;

        switch (lang)
        {
            case "English":
                return Languages.English;
            case "Portuguese":
                return Languages.Portuguese;
            case "German":
                return Languages.German;
            case "Spanish":
                return Languages.Spanish;
            default:
                return Languages.English;
        }
    }

    public static string GetCurrentLanguageCode()
    {
        string lang = PlayerPrefs.GetString("language");

        if (string.IsNullOrEmpty(lang)) lang = "English";

        switch (lang)
        {
            case "Spanish": return "ES";
            case "Portuguese": return "PT";

            default: return "EN";
        }
    }

    private static void Read()
    {
        dialogData.Clear();


        TextAsset asset = Resources.Load<TextAsset>("Files/dialogs");

        string contents = string.Empty;

        if (asset != null)
            contents = asset.text;

        bool fileExists = File.Exists(path);

        // convert string to stream
        byte[] byteArray = Encoding.UTF8.GetBytes(contents);
        //byte[] byteArray = Encoding.ASCII.GetBytes(contents);
        MemoryStream stream = new MemoryStream(byteArray);


        using (StreamReader sr = fileExists ? new StreamReader(path) : new StreamReader(stream))
        {
            string line;

            int lineCount = 0;

            while (sr.EndOfStream == false)
            {

                lineCount++;

                //descarta a primeira linha (header)
                line = sr.ReadLine();
                line.Trim();
                string[] val = line.Split(';');



                DialogData data = new DialogData();

                //if())//pula a primeira linha
                //	continue;
                //Debug.Log(lineCount+" -> "+val[0]+" "+val[1]+" "+val[3]);
                //Debug.Log(lineCount+" -> "+val[0]);
                data.dialogID = val[0];
                data.language = val[1];
                data.text = val[2];
                data.audioFile = val[3];

                PopulateDialogData(data);
            }

            Debug.Log("Total de linhas: " + lineCount.ToString());
        }

        if (fileExists)
            Debug.LogWarning("Dialogs loaded from data path ");
        else
            Debug.LogWarning("Dialogs loaded from assets");
    }

    private static void PopulateDialogData(DialogData data)
    {
        dialogData.Add(data);
    }
    //Lucas
    //English version has one more dialog - 73DA. Spanish is missing.
    //===
}
