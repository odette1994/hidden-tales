﻿public enum GameMode
{
	EDITOR = 0,
	GAME,
	GAME_TIMEATTACK,
	GAME_EASY
}

public enum GameState
{
	MENU = 0,
	LEVEL_INTRO,
	IN_GAME,
	LEVEL_END,
	PAUSE

}

public enum Layers
{
	GAME = 32,
	GAME_UI = 50
}

public enum Characters
{
	ALISE = 0,
	DEREK,
	BROMLEY,
	UBERTA,
	ODETTE,
	JEAN_BOB,
	LORD_ROGERS,
	LUCAS,
    ALISE_AND_LUCAS,
    PUFFIN,
    ROTHBART,
    SCULLY,
    SPEED
}

public enum LevelResult
{
	NONE = 0, //nunca terminou o level
	WIN,
	TIMEUP,
	BOGG_TIMEUP
}