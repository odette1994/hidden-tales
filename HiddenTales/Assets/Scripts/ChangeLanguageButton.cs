﻿using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;

public class ChangeLanguageButton : MonoBehaviour
{
    [SerializeField] private Sprite english;
    [SerializeField] private Sprite spanish;

    [SerializeField] private Image flagIcon;

    private void OnEnable()
    {
        Dialogs.OnLanguageChanged += LanguageChanged;
    }

    private void OnDisable()
    {
        Dialogs.OnLanguageChanged -= LanguageChanged;
    }

    private void Start()
    {
        LanguageChanged(Dialogs.GetCurrentLanguage());
    }

    private void LanguageChanged(Languages newLanguage)
    {
        switch (newLanguage)
        {
            case Languages.Spanish:
                flagIcon.sprite = spanish;
                break;
            default:
                flagIcon.sprite = english;
                break;
        }
    }

    public void CycleLanguage()
    {
        if (Dialogs.GetCurrentLanguage() == Languages.English)
            Dialogs.SetLanguage(Languages.Spanish);
        else
            Dialogs.SetLanguage(Languages.English);
    }
}
