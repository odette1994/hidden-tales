﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	public AudioSource sfxAudioSource;
	public AudioSource bgmAudioSource;
	public AudioSource dialogsAudioSource;

	public static float sfxVolume {get; private set;}
	public static float dialogsVolume {get; private set;}
	public static float bgmVolume {get; private set;}

	public static bool isPlayingDialog{get; private set;}

	public static AudioClip[] sfxClips{get; private set;}
	//public static AudioClip[] dialogClips{get; private set;}
	//public static AudioClip[] bgmClips{get; private set;}

	private static SoundManager _instance;
	public static SoundManager instance
	{
		get{

			if(_instance == null){

				//busca o prefab na resources
				SoundManager obj = Resources.Load<SoundManager>("Prefabs/SoundManager");

				obj = Instantiate(obj) as SoundManager;

				obj.name = "~SoundManager";

				DontDestroyOnLoad(obj.gameObject);

				_instance = obj;

			}

			//_instance.Init();

			return _instance;
		}


	}

	void Awake()
	{
		if(_instance == null){
			_instance = this;
			DontDestroyOnLoad(gameObject);

		}
		else{
			Destroy(gameObject);
			return;
		}

		if(sfxClips == null)
			Init();



		sfxVolume = PlayerPrefs.GetFloat("sfx", 1);
		bgmVolume = PlayerPrefs.GetFloat("bgm", 1);

		sfxAudioSource.volume = sfxVolume;
		bgmAudioSource.volume = bgmVolume;
		dialogsAudioSource.volume = sfxVolume;//dialogsVolume;

		//Debug.Log("Loaded sfx: " + sfxVolume.ToString() + " / BGM: " + bgmVolume.ToString());

	}


	/// <summary>
	/// Carrega todos os audiclips da Resources.
	/// Deve ser chamado pela cena de Load
	/// </summary>
	public static void Init()
	{
		sfxClips = Resources.LoadAll<AudioClip>("SFX");

	}

	void Update()
	{
		if(_instance == null)
			return;
		


		isPlayingDialog = dialogsAudioSource.isPlaying;

	}

	public static void PlaySFX(string sound)
	{

		AudioClip clip = null;

		foreach (var item in sfxClips) {
			if(item.name.ToLower().Contains(sound)){
				clip = item;
				break;
			}
		}

		if(clip != null){
			instance.sfxAudioSource.PlayOneShot(clip);
			//instance.sfxAudioSource.clip = clip;
			//instance.sfxAudioSource.Play();
		}
	}

	/// <summary>
	/// Solicita um diálogo para tocar, pode ser pausado
	/// </summary>
	/// <param name="name">Name.</param>
	/// <param name="pause">If set to <c>true</c> pause.</param>
	public static void PlayDialog(string name)
	{
		
		AudioClip clip = Resources.Load<AudioClip>($"VoiceOver/{Dialogs.GetCurrentLanguageCode()}/"+name);

		if(clip != null){
			instance.dialogsAudioSource.clip = clip;
			instance.dialogsAudioSource.Play();
		}
	}

	public static void PlayBGM(string character)
	{
		instance.StopAllCoroutines();

		//Verifica se já não está tocando o mesmo som
		if(instance.bgmAudioSource.clip !=null)
			if(instance.bgmAudioSource.clip.name.Contains(character))
				return;

		if (character == "ALISE_AND_LUCAS" || character == "PUFFIN" || character == "ROTHBART" || character == "SCULLY" || character == "SPEED")
		{
			string[] stringArray = new string[] { "ALISE", "BROMLEY", "DEREK", "UBERTA" };
			character = stringArray[Random.Range(0, stringArray.Length)];

        }
        else if (character.Contains("BOB") || character.Contains("ROGERS") || character.Contains("LUCAS"))
			character = "ALISE";

		//TEMA-ALISE
		//TEMA-BROMLEY
		//TEMA-BROMLEY
		//TEMA-BROMLEY

		string c = "BGM/TEMA-"+character;

		//Debug.Log(c);

		AudioClip clip = Resources.Load<AudioClip>(c);

		if(clip != null){
			instance.bgmAudioSource.clip = clip;
			instance.StartCoroutine(instance.FadeInBGM());
			instance.bgmAudioSource.Play();
		}

	}


	public static void StopBGM()
	{
		instance.StopAllCoroutines();
		instance.StartCoroutine(instance.FadeOutBGM());
	}
		

	public static float GetDialogClipLength()
	{
		if(instance.dialogsAudioSource.clip == null)
			return 0;
		
		return instance.dialogsAudioSource.clip.length;
	}

	public static void SetVolume(float sfx, float bgm)
	{
		bgmVolume = bgm;
		sfxVolume = sfx;

		instance.sfxAudioSource.volume = sfxVolume;
		instance.bgmAudioSource.volume = bgmVolume;
		instance.dialogsAudioSource.volume = sfxVolume;
	}

	public static void SaveVolume()
	{
		PlayerPrefs.SetFloat("sfx", sfxVolume);
		PlayerPrefs.SetFloat("bgm", bgmVolume);

		instance.sfxAudioSource.volume = sfxVolume;
		instance.bgmAudioSource.volume = bgmVolume;
		instance.dialogsAudioSource.volume = sfxVolume;//dialogsVolume;

		PlayerPrefs.Save();

		//Debug.Log("saved sfx: " + sfxVolume.ToString() + " / BGM: " + bgmVolume.ToString());
	}



	private IEnumerator FadeOutBGM()
	{
		
		while (bgmAudioSource.volume > 0) {
			bgmAudioSource.volume -= Time.deltaTime;
			yield return null;
		}

		bgmAudioSource.Stop();
		bgmAudioSource.clip = null;
	}

	private IEnumerator FadeInBGM()
	{
		bgmAudioSource.volume = 0;
		float vol = PlayerPrefs.GetFloat("bgm", 1);

		bgmAudioSource.Play();

		while (bgmAudioSource.volume < vol) {
			bgmAudioSource.volume += Time.deltaTime;
			yield return null;
		}


	}
}
