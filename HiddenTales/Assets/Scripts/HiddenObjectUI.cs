﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HiddenObjectUI : MonoBehaviour {

	public HiddenObject hiddenObject;
	public Image image;
	public Color foundColor;

	// Use this for initialization
	void Awake () {
		image = GetComponent<Image>();
	}

	public void Init(HiddenObject obj)
	{
		hiddenObject = obj;
		image.sprite = obj.hudSprite;

		hiddenObject.OnDestroyed += OnOriginalObjDestroy;
	}

	void OnOriginalObjDestroy()
	{
		//Debug.Log("destoyed");
		Destroy(this.gameObject);

	}


	void OnDestroy()
	{
		if(hiddenObject == null)
			return;
			
		hiddenObject.OnDestroyed -= OnOriginalObjDestroy;
	}

}
