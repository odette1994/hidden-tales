using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Purchasing;

public class IAPManager : MonoBehaviour
{
#if UNITY_ANDROID
    private Dictionary<string, string[]> _productPackages = new Dictionary<string, string[]> // Use store package product.
    {
        { "com.swanprincess.hiddenobjects.fullpack001a", new string[] { "com.swanprincess.hiddenobjects.jbs001", "com.swanprincess.hiddenobjects.ls001", "com.swanprincess.hiddenobjects.lrs001" } },
        { "com.swanprincess.hiddenobjects.fullpack002", new string[] { "com.swanprincess.hiddenobjects.aal001", "com.swanprincess.hiddenobjects.pfn001", "com.swanprincess.hiddenobjects.rbt001" } },
        { "com.swanprincess.hiddenobjects.fullpackmix001", new string[] { "com.swanprincess.hiddenobjects.jbs001", "com.swanprincess.hiddenobjects.ls001", "com.swanprincess.hiddenobjects.lrs001", "com.swanprincess.hiddenobjects.aal001", "com.swanprincess.hiddenobjects.pfn001", "com.swanprincess.hiddenobjects.rbt001" } }
    };
#elif UNITY_IOS
    private Dictionary<string, string[]> _productPackages = new Dictionary<string, string[]> // Use store package product.
    {
        { "com.swanprincess.hiddenobjects.fullpack001", new string[] { "com.swanprincess.hiddenobjects.jbs001", "com.swanprincess.hiddenobjects.ls001", "com.swanprincess.hiddenobjects.lrs001" } },
        { "com.swanprincess.hiddenobjects.fullpack002", new string[] { "com.swanprincess.hiddenobjects.aal001", "com.swanprincess.hiddenobjects.pfn001", "com.swanprincess.hiddenobjects.rbt001" } },
        { "com.swanprincess.hiddenobjects.fullpackmix001", new string[] { "com.swanprincess.hiddenobjects.jbs001", "com.swanprincess.hiddenobjects.ls001", "com.swanprincess.hiddenobjects.lrs001", "com.swanprincess.hiddenobjects.aal001", "com.swanprincess.hiddenobjects.pfn001", "com.swanprincess.hiddenobjects.rbt001" } }
    };
#endif

    private Dictionary<string, int> _productPackagesLimits = new Dictionary<string, int> // Use for prevent accident unfair buy product.
    {
        { "com.swanprincess.hiddenobjects.fullpack001a", 1 },//e.g if user buy "jbs001", "ls001", this button has disable ( buy > 1 of products line )
        { "com.swanprincess.hiddenobjects.fullpack002", 1 },
        { "com.swanprincess.hiddenobjects.fullpackmix001", 3}//e.g if user buy "jbs001", "ls001", "lrs001", "aal001", this button has disable ( buy > 3 of products line  )
    };

    [SerializeField] private List<IAPButton> _purchaseButtons;
    [SerializeField] private ChapterSelect _chapterSelect;

    private void Start()
    {
        UpdateAllPurchaseButtonsState();
    }

    private void UpdateAllPurchaseButtonsState()
    {
        for(int i = 0; i < _purchaseButtons.Count; ++i)
        {
            string productId = _purchaseButtons[i].productId;
            bool isOwned = IsProductOwned(productId);
            if (_productPackagesLimits.ContainsKey(productId))
            {
                int numberOfStoriesBought = 0;
                for (int storyIndex = 0; storyIndex < _productPackages[productId].Length; ++storyIndex)
                {
                    if (PlayerPrefs.GetInt(_productPackages[productId][storyIndex]) > 0)
                        numberOfStoriesBought++;
                }

                bool isAboveStoriesLimit = numberOfStoriesBought > _productPackagesLimits[productId];

                _purchaseButtons[i].gameObject.SetActive(!isAboveStoriesLimit && !isOwned);
            }
            else
                _purchaseButtons[i].gameObject.SetActive(!isOwned);
        } 
    }

    public void ProcessPurchase(Product product)
    {
        PlayerPrefs.SetInt(product.definition.id, 1);

        foreach (var package in _productPackages)
        {
            if(package.Key.Equals(product.definition.id))
            {
                for(int i = 0; i < package.Value.Length; ++i)
                    PlayerPrefs.SetInt(package.Value[i], 1);
            }
        }

        PlayerPrefs.Save();

        _chapterSelect.CalculateStars();
        UpdateAllPurchaseButtonsState();
    }

    public bool IsProductOwned(string productId)
    {
        Debug.Log("Testing " + productId + ": " + PlayerPrefs.GetInt(productId));
        if (PlayerPrefs.GetInt(productId) > 0)
            return true;

        foreach (var package in _productPackages)
        {
            if (PlayerPrefs.GetInt(package.Key) > 0 && package.Value.ToList().Contains(productId))
                return true;
        }

        return false;
    }
}
