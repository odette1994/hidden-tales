﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;

public class Fade : MonoBehaviour {

	private static Fade _instance;
	private static bool onFade;

	private static CanvasGroup canvasGroup;
	private static Canvas canvas;

	public static Fade instance
	{
		get{

			if(_instance == null){

				_instance = Init();

			}

			return _instance;
		}
	}

	// Use this for initialization
	void Awake () {

		//if(instance != null){
		//	Destroy(gameObject);
		//	return;
		//}



	}

	static Fade Init()
	{
		//busca o prefab na resources
		GameObject go = new GameObject("~Fade");
		Fade fade = go.AddComponent<Fade>();

		canvas = go.AddComponent<Canvas>();

		canvas.renderMode = RenderMode.ScreenSpaceOverlay;

		canvasGroup = go.AddComponent<CanvasGroup>();

		canvasGroup.interactable = false;

		canvasGroup.blocksRaycasts = false;

		Image image = go.AddComponent<Image>(); 

		image.color = Color.black;

		DontDestroyOnLoad(fade.gameObject);

		return fade;
	}


	public static void FadeIn(float time, int sortOrder = 100, System.Action callback = null)
	{
		//instance.canvasGroup.gameObject.SetActive(true);

		if(_instance == null)
			_instance = Init();

		canvasGroup.alpha = 1;

		LeanTween.alphaCanvas(canvasGroup, 0, time).setOnComplete(()=>{
			if (callback != null)
				callback();

			//instance.canvasGroup.gameObject.SetActive(false);
		});

		canvas.sortingOrder = sortOrder;



	}

	public static void FadeOut(float time, int sortOrder = 100, System.Action callback = null)
	{
		//instance.canvasGroup.gameObject.SetActive(true);

		if(_instance == null)
			_instance = Init();

		LeanTween.alphaCanvas(canvasGroup, 1, time).setOnComplete(()=>{
			if (callback != null)
				callback();

			//instance.canvasGroup.gameObject.SetActive(false);
		});

		canvas.sortingOrder = sortOrder;


		//instance.StartCoroutine(instance.CoFadeOut(time, sortOrder, callback));
	}


	IEnumerator CoFadeOut(float time, int sortOrder, System.Action callback = null)
	{
		if(onFade)
			yield break;

		onFade = true;

		canvas.sortingOrder = sortOrder;

		float timer = 0;
		float normalizedTime = 0;

		while (normalizedTime < 1) {

			timer += Time.deltaTime;

			normalizedTime = timer / time;

			canvasGroup.alpha = normalizedTime;

			yield return null;
		}

		canvasGroup.alpha = 1;

		onFade = false;

		if (callback != null)
			callback();

		yield return null;
	}

	public static void Hide()
	{
		if(_instance == null)
			_instance = Init();
		canvasGroup.alpha = 0;
	}
}
