﻿using UnityEngine;
using System.Collections;

public class CameraFit : MonoBehaviour {

	public float aspect;
	public float width;
	public float height;

	Camera cam;

	// Use this for initialization
	void Awake () {
		cam = gameObject.GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
		width = Screen.width;
		height = Screen.height;

		aspect = width/height;

		if(aspect > 1.35f) //16:9 
		{
			cam.orthographicSize = 6.45f;
			cam.transform.position = new Vector3(9.6f, 6.09f, -10);
		}
		else //4:3
		{
			cam.orthographicSize = 7.2f;
			cam.transform.position = new Vector3(9.6f, 5.34f, -10);
		}
	}
}
