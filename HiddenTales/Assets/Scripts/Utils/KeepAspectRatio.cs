﻿using UnityEngine;
using System.Collections;

public class KeepAspectRatio : MonoBehaviour {

	private static Camera cam;

	private static Camera camUI;

	private static Vector2 resolution;
	private static Vector2 oldResolution;

	private static bool changingAspect;

	private static LayerMask storedMask;

	// Use this for initialization
	void Start () 
	{
		// obtain camera component so we can modify its viewport
		cam = GetComponent<Camera>();

		changingAspect = false;

		UpdateAspect();
	}

	public static void SetCameraUI(Camera _camUI)
	{
		camUI = _camUI; 
	}

	void OnDestroy()
	{
		resolution = Vector2.zero;
		oldResolution = Vector2.zero;
	}

	void Update()
	{

		resolution.x = Screen.width;

		resolution.y = Screen.height;


		if(Vector2.Equals(oldResolution, resolution) == false)
		{
			if(changingAspect)
				return;

			changingAspect = true;

			storedMask = cam.cullingMask;

			cam.cullingMask = 0;

			StartCoroutine(UpdateAspect());

			//Debug.Log("Alteração de resolução detectada");
		}

	}

	public static IEnumerator UpdateAspect()
	{

		yield return new WaitForEndOfFrame();

		// set the desired aspect ratio (the values in this example are
		// hard-coded for 16:9, but you could make them into public
		// variables instead so you can set them at design time)
		float targetaspect = 4.0f / 3.0f;

		// determine the game window's current aspect ratio
		float windowaspect = (float)Screen.width / (float)Screen.height;

		// current viewport height should be scaled by this amount
		float scaleheight = windowaspect / targetaspect;

		// if scaled height is less than current height, add letterbox
		if (scaleheight < 1.0f)
		{  
			Rect rect = cam.rect;

			rect.width = 1.0f;
			rect.height = scaleheight;
			rect.x = 0;
			rect.y = (1.0f - scaleheight) / 2.0f;

			cam.rect = rect;

			if(camUI)
				camUI.rect = rect;
		}
		else // add pillarbox
		{
			float scalewidth = 1.0f / scaleheight;

			Rect rect = cam.rect;

			rect.width = scalewidth;
			rect.height = 1.0f;
			rect.x = (1.0f - scalewidth) / 2.0f;
			rect.y = 0;

			cam.rect = rect;

			if(camUI)
				camUI.rect = rect;
		}

		cam.cullingMask = storedMask;

		oldResolution = resolution;

		changingAspect = false;

	}
}
