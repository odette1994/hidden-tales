﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MyButton : MonoBehaviour {

	Button button;
	Text text;

	// Use this for initialization
	void Start () {
	
	}

	void OnEnable()
	{
		text = GetComponentInChildren<Text>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public string GetText()
	{
		return text.text;
	}

	public void FillFileValue()
	{
		LevelEditorUI.instance.levelID.text = text.text;
	}

	public void SetText(string text)
	{
		if(this.text == null)
			this.text = GetComponentInChildren<Text>();
		this.text.text = text;
	}
}
