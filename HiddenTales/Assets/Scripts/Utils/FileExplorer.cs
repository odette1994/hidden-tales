﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using PlayerPrefs = PreviewLabs.PlayerPrefs;

public class FileExplorer : MonoBehaviour {

	public MyButton originalButton;

	bool alreadyMounted;

	List<MyButton> buttonList = new List<MyButton>();

	void OnEnable()
	{
		PlayerPrefs.OnSave += OnPlayerPrefsChanged;
	}

	void OnDisable()
	{
		PlayerPrefs.OnSave -= OnPlayerPrefsChanged;
	}

	// Use this for initialization
	void Start () {

		MountButtons();

	}

	void MountButtons()
	{
		//pega todas as keys do playerprefs
		List<string> levels = new List<string>(SaveLoad.GetAllLevels());

		//procura pelos levels que não existem mas existe botoes (caso de deletar um level)
		for (int i = 0; i < buttonList.Count; i++) {

			bool exists = false;

			foreach (var item in levels) {
				if(buttonList[i].GetText() == item){
					exists = true;
					originalButton = buttonList[i];//nao queremos perder o botao padrao
					break;
				}
			}

			if(exists == false)
			{
				Destroy(buttonList[i].gameObject);
				buttonList.RemoveAt(i);
			}

		}

		//Ordena botoes
		levels.Sort();

		//Spawna novos botoes caso nao existam ainda
		foreach (var item in levels) {

			bool exists = false;

			//procura se ja existe o botao
			foreach (var btn in buttonList) {
				if(btn.GetText() == item){
					exists = true;
					break;
				}
			}

			if(!exists)
				SpawnButton(item);
		}


		//Aqui controla apenas o botao original para spawnar novos botoes
		if(alreadyMounted)
			return;

		if(buttonList.Count > 0){
			Destroy(originalButton.gameObject);
			originalButton = buttonList[0];
		}

		alreadyMounted = true;
		//-----
	}
	
	// Update is called once per frame
	//void Update () {
	
	//}

	void SpawnButton(string text)
	{
		MyButton btn = Instantiate(originalButton, Vector3.zero, Quaternion.identity) as MyButton;
		btn.transform.SetParent(transform, false);
		buttonList.Add(btn);
		btn.SetText(text);
	}

	void OnPlayerPrefsChanged()
	{
		MountButtons();
	}

}
