﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

[System.Serializable]
public class GameSettings{

	//valores default

	public float f_timeLimit = 90; //5 minutos default
	public float f_timeLimitEasyMode = 300;
	public float f_blinkAfter = 20; //objeto pisca depois de xx segundos depois do ultimo ser encontrado
	public int i_minObjects = 12; //numero minimo de objetos clicaveis
	public float f_timePenaltyError = 5; //desconto de tempo quando erra no modo normal
	public float f_timePenaltyErrorEasyMode = 2;
	public float f_timePnaltyErrorTimeAttack = 5; //desconto de tempo quando erra no modo time attack
	public float f_minImportantObjects = 33; //percentagem de objetos importantes presentes no level

	//Boggs
	public float f_timeToFindBoggs;
	public int i_first_bogg_appears_after_objects;
	public float f_first_bogg_appears_after_seconds;
	public int i_second_bogg_appears_after_objects;
	public float f_second_bogg_appears_after_seconds;


}

public static class Utils {


	public static SystemLanguage GetLanguage()
	{
		return Application.systemLanguage;
	}

	/// <summary>
	/// Retorna o game settings do .ini ou default
	/// </summary>
	/// <returns>The game settings.</returns>
	public static GameSettings GetGameSettings()
	{
		GameSettings settings = new GameSettings();

		settings.f_timeLimit = IniFile.GetFloat("Settings", "Time_Limit", 0);
		settings.f_timeLimitEasyMode = IniFile.GetFloat("Settings", "Time_Limit_Easy_Mode", 0);
		settings.f_blinkAfter = IniFile.GetFloat("Settings", "Blink_After", 0);
		//settings.i_finds = IniFile.GetInt("Settings", "Finds", settings.i_finds);
		settings.i_minObjects = IniFile.GetInt("Settings", "Minimum_Objects", 0);
		settings.f_timePenaltyError = IniFile.GetFloat("Settings", "Time_Penalty_Error", 0);
		settings.f_timePenaltyErrorEasyMode = IniFile.GetFloat("Settings", "Time_Penalty_Error_EasyMode", 0);
		settings.f_minImportantObjects = IniFile.GetFloat("Settings", "Importants_Objects_Percentage", 0);
		settings.f_timePnaltyErrorTimeAttack = IniFile.GetFloat("Settings", "Time_Penalty_Error_TimeAttack", 0);

		//Boggs
		settings.f_timeToFindBoggs = IniFile.GetFloat("Settings", "time_limit_to_find_bogg", 0);
		settings.i_first_bogg_appears_after_objects = IniFile.GetInt("Settings", "first_bogg_appears_after_objects", 0);
		settings.f_first_bogg_appears_after_seconds = IniFile.GetFloat("Settings", "first_bogg_appears_after_seconds", 0);
		settings.i_second_bogg_appears_after_objects = IniFile.GetInt("Settings", "second_bogg_appears_after_objects", 0);
		settings.f_second_bogg_appears_after_seconds = IniFile.GetFloat("Settings", "second_bogg_appears_after_seconds", 0);

		return settings;
	}
	
	public static string GetNiceCharacterName(Characters character)
	{
		string name = character.ToString().ToLower();

		string result = string.Empty;

		for (int i = 0; i < name.Length; i++) {

			if(i == 0)
				result += name[i].ToString().ToUpper();
			else
			if(name[i].ToString() == "_")
				result += " ";
				else
					if (name[i-1] == '_')
						result += name[i].ToString().ToUpper();
				else result += name[i];


		}

		return result;

	}

	public static string GetCharacterName(int id)
	{
		return System.Enum.Parse(typeof(Characters), id.ToString()).ToString();
	}

	public static int GetCharaterID(string character)
	{
		return (int)System.Enum.Parse(typeof(Characters), character);
	}

}


