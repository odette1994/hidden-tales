﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;

public class IniFile
{
	private static string path = Application.persistentDataPath + "/game_ini.txt";

	private static GameSettings gameSettings;
	private static Dictionary<string, Dictionary<string, string>> IniDictionary = new Dictionary<string, Dictionary<string, string>>();

	private static bool initialized = false;

	private static bool FirstRead ()
	{
		
		TextAsset content = Resources.Load<TextAsset>("Files/game_ini");
		Stream s = GenerateStreamFromString(content.text);

		bool fileExists = File.Exists (path);
			
		using (StreamReader sr = fileExists? new StreamReader (path) : new StreamReader (s) ) {
			string line;
			string section = "";
			string key = "";
			string value = "";


			//while (!string.IsNullOrEmpty(line = sr.ReadLine()))
			while (sr.EndOfStream == false) {
				line = sr.ReadLine ();
				line.Trim ();

				if (line.StartsWith ("#") || line.Length == 0)
					continue;

				if (line.StartsWith ("[") && line.EndsWith ("]")) {
					section = line.Substring (1, line.Length - 2);
				} else {
					string[] ln = line.Split (new char[] { '=' });
					key = ln [0].Trim ();
					value = ln [1].Trim ();
				}
				if (section == "" || key == "" || value == "")
					continue;
				PopulateIni (section, key, value);
			}
		}

		Debug.Log(IniDictionary.Count);
			
		if(fileExists)
			Debug.Log("Ini file loaded from file");
		else
			Debug.Log("Ini file loaded from assets");

		initialized = true;
		return true;


	}

	public static Stream GenerateStreamFromString(string s)
	{
		MemoryStream stream = new MemoryStream();
		StreamWriter writer = new StreamWriter(stream);
		writer.Write(s);
		writer.Flush();
		stream.Position = 0;
		return stream;
	}

	private static void PopulateIni(string _section, string _key, string _value)
	{
		
		if(IniDictionary.Keys.Contains(_section))
		{
			//Debug.Log("Added:" + _key + "/" + _value);
			IniDictionary[_section].Add(_key, _value);

		}
		else
		{
			Dictionary<string, string> newVal = new Dictionary<string, string>();
			newVal.Add(_key.ToString(), _value);
			IniDictionary.Add(_section.ToString(), newVal);
			//Debug.Log("Added:" + _key + "/" + _value);
		}
	}

	public static int GetInt(string _Section, string _Key, int defaultValue)
	{


		if (!initialized)
			FirstRead ();
		if (IniDictionary.ContainsKey (_Section)) {

			if (IniDictionary [_Section].ContainsKey (_Key)) {

				int val = int.Parse (IniDictionary [_Section] [_Key]);
				//Debug.Log ("Returning found value: " + val);
				return val;
			} else {
				//IniWriteValue(_Section, _Key, defaultValue);
				//Debug.Log ("Default value: " + defaultValue);
				return defaultValue;
			}

		} else {
			//IniWriteValue(_Section, _Key, defaultValue);
			//Debug.Log("Default value no Section: " + defaultValue);
			return defaultValue;
		}

			
	}

	public static float GetFloat(string _Section, string _Key, float defaultValue)
	{


		if (!initialized)
			FirstRead ();
		if (IniDictionary.ContainsKey (_Section)) {

			if (IniDictionary [_Section].ContainsKey (_Key)) {
				float val = float.Parse (IniDictionary [_Section] [_Key]);
				//Debug.Log ("Returning found value: " + val);
				return val;
			} else {
				//IniWriteValue(_Section, _Key, defaultValue);
				//Debug.Log ("Default value: " + defaultValue);
				return defaultValue;
			}

		} else {
			//IniWriteValue(_Section, _Key, defaultValue);
			//Debug.Log("Default value no Section: " + defaultValue);
			return defaultValue;
		}


	}

}
