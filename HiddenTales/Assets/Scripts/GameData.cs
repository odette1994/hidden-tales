﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Estatísticas de um determinado level
/// </summary>
[System.Serializable]
public class GameLevelStatistics
{

    public string levelID;
    public int character;
    public int starsCount;
    public int bestStar;
    public float bestTime; //melhor tempo no timeattack
    public LevelResult levelResult;

    public bool HasLostLevel()
    {
        return levelResult == LevelResult.BOGG_TIMEUP || levelResult == LevelResult.TIMEUP;
    }

}

/// <summary>
/// Armazena as estatísticas de levels para ser salvo na PlayerPrefs
/// </summary>
[System.Serializable]
public class GameLevelStisticsData
{
    public List<GameLevelStatistics> levelsStatistics = new List<GameLevelStatistics>();

    public bool ContainsLevel(string levelID)
    {
        foreach (var item in levelsStatistics)
        {
            if (item.levelID.Equals(levelID))
                return true;
        }

        return false;
    }

    /// <summary>
    /// Obtem o número de estrelas de um certo personagem
    /// </summary>
    /// <returns>The star count.</returns>
    /// <param name="character">Character.</param>
    public int GetStarCount(int character)
    {
        int count = 0;

        foreach (var item in levelsStatistics)
        {

            if (item.character.Equals(character))
                count += item.starsCount;

        }

        return count;

    }


    public bool GetIsCompleateCharaterLevel(int character)
    {
        Dictionary<string, int> stagesStarMax = new Dictionary<string, int>();
        foreach (var item in levelsStatistics)
        {

            if (item.character.Equals(character) )// && item.bestStar > 0 )
            {

                //if (stagesStarMax.ContainsKey(item.levelID)) {
                //    if( item.bestStar > stagesStarMax[item.levelID] )
                //        stagesStarMax[item.levelID] = item.bestStar;
                //} else {
                    stagesStarMax[item.levelID] = item.bestStar;
                //}

            }

        }
        Debug.Log("GetIsCompleateCharaterLevel ###################");
        int total = 0;
        foreach (KeyValuePair<string, int> entry in stagesStarMax)
        {
            Debug.Log("entry.Key:"+ entry.Key + " entry.Value:"+ entry.Value);
            total += entry.Value;
        }
        if (total > 14)
            return true;

        return false;

    }



    

    /// <summary>
    /// Obtém o número total de estrelas de todo o jogo
    /// </summary>
    /// <returns>The star count.</returns>
    public int GetStarCount()
    {
        int count = 0;

        foreach (var item in levelsStatistics)
        {
            count += item.starsCount;
        }

        return count;

    }

    public GameLevelStatistics GetLevelStatistics(string levelID)
    {
        foreach (var item in levelsStatistics)
        {
            if (item.levelID.Contains(levelID))
                return item;
        }

        return new GameLevelStatistics();
    }

    public void SetLevelStatistic(GameLevelStatistics statistic, out bool newBest)
    {
        newBest = false;
        for (int i = 0; i < levelsStatistics.Count; i++)
            if (levelsStatistics[i].levelID.Equals(statistic.levelID))
            {
                levelsStatistics[i].starsCount += statistic.starsCount;//Adiciona estrelas ganhas
                if( levelsStatistics[i].bestStar == 0 || statistic.starsCount > levelsStatistics[i].bestStar)
                    levelsStatistics[i].bestStar = statistic.starsCount;
                if (levelsStatistics[i].bestTime == 0 || levelsStatistics[i].bestTime > statistic.bestTime)
                {
                    newBest = true;
                    levelsStatistics[i].bestTime = statistic.bestTime;
                }
                return;
            }

        //Se não encontrou o level cria um novo
        levelsStatistics.Add(statistic);
    }
}
