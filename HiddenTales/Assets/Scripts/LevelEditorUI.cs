﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class LevelEditorUI : MonoBehaviour {

	public static LevelEditorUI instance;

	public InputField levelID;
	public Image currentObjImg;
	public Slider scaleSlider;
	public Slider transparencySlider;
	public Toggle enableShadow;
	public Toggle enableImportance;
	public Toggle enableIsBoss;
	public Toggle enableLock;
	public Toggle Toggle_enableBogg;
	public Text txt_objName;
	public Text txt_objSort;
	public RectTransform mainRect;
	public Button btn_PlayStop;
	public Button btn_Adv;
	public Button btn_hideShow;

	public RectTransform levelBrowserRect;

	public CanvasGroup notificationCanvasGroup;
	public CanvasGroup advLvlCanvasGroup;
	public Text notificationText;

	public Camera zoomCamera;

	//adv level
	[Header("Advanced")]
	public Dropdown lvlOnWinDropdown;
	public Dropdown lvlOnLoseDropdown;
	public InputField InputField_introDialog;
	public InputField InputField_winDialog;
	public InputField InputField_defeatDialog;
	public Dropdown Dropdown_character;
	public InputField InputField_findsCount;
	public Toggle Toggle_timeAttack;

	private CanvasGroup mainRectCanvasGroup;


	int objIndex = 0;
	int chapIndex = 0;
	int bgIndex = 0;
	bool isCameraZoomOn;

	bool isTestingLevel;

	//Private vars
	[SerializeField]
	private HiddenObject currentObj; //objeto que já está na cena
	private HiddenObject currentUIObj; //objeto a ser spawnado


	public List<HiddenObject> objectsList = new List<HiddenObject>();//lista de prefabs


	private Vector2 mainRectInitialPos;

	void Awake()
	{
		instance = this;

		mainRectCanvasGroup = mainRect.GetComponent<CanvasGroup>();

		GameManager.OnLevelLoaded += OnLevelLoaded;
	}

	// Use this for initialization
	IEnumerator Start () {


		//Desabilita todo o editor caso esteja em modo game
		if(GameManager.currentGameMode > 0)
		{
			mainRect.gameObject.SetActive(false);
			zoomCamera.gameObject.SetActive(false);
			notificationCanvasGroup.alpha = 0;
			btn_PlayStop.gameObject.SetActive(false);
			btn_Adv.gameObject.SetActive(false);
			btn_hideShow.gameObject.SetActive(false);
			this.enabled = false;

			yield break;
		}

		GameManager.FillObjectsListEditor();

		mainRectInitialPos = mainRect.anchoredPosition;

		//Seta o jogo como modo editor
		//GameManager.currentGameMode = GameMode.EDITOR;

		yield return new WaitForEndOfFrame();

		//seleciona o primeiro set de objetos do primeiro cenario
		MoveChapIndex(1);

		isCameraZoomOn = true;

		zoomCamera.gameObject.SetActive(isCameraZoomOn);

		notificationCanvasGroup.alpha = 0;

		MoveBGIndex(1);

		List<string> options = new List<string>();

		foreach (Characters val in System.Enum.GetValues(typeof(Characters)))
		{
			options.Add(val.ToString());
		}

		Dropdown_character.AddOptions(options);

		OpenAdvPnl(false);

		OnScreenClick.OnClick += OnClick;

		ConfirmationWindow.OnWindowClosed += OnWindowClosed;
		ConfirmationWindow.OnWindowRised += OnWindowRised;
	}

	void Notify(string text)
	{
		if(notifyCoroutine != null)
			StopCoroutine(notifyCoroutine);

		notifyCoroutine = StartCoroutine(CoNotify(text));
	}

	Coroutine notifyCoroutine;
	IEnumerator CoNotify(string text)
	{
		float duration = 1.7f;
		float timer = 0;
		float normalizedTime = 0;

		notificationText.text = text;

		notificationCanvasGroup.alpha = 1;

		yield return new WaitForSeconds(1.3f);

		while(normalizedTime < 1)
		{
			timer += Time.deltaTime;
			normalizedTime = timer / duration; 

			notificationCanvasGroup.alpha = 1-normalizedTime;
			yield return null;
		}
	}

	HiddenObject lastObj;
	// Update is called once per frame
	void Update () {

		//if(Input.GetKeyDown(KeyCode.Space))
		//	Test();
		
		if(isTestingLevel)
		{
			mainRectCanvasGroup.alpha = 0;
			mainRectCanvasGroup.interactable = false;

			if(zoomCamera.gameObject.activeSelf == true)
				zoomCamera.gameObject.SetActive(false);
			
			return;
		}
		
		//se estiver editando o nome do level, não continua
		if(levelID.isFocused)
			return;

		//atalho de sort order
		if(currentObj != null)
		{
			if(Input.GetKeyDown(KeyCode.Comma))
				SortOrder(-1);
			if(Input.GetKeyDown(KeyCode.Period))
				SortOrder(1);
		}

		//atalho is locked
		if(Input.GetKeyDown(KeyCode.L))
			enableLock.isOn = !enableLock.isOn;

		//atalho is boss
		if(Input.GetKeyDown(KeyCode.B))
			enableIsBoss.isOn = !enableIsBoss.isOn;

		//atalho is important
		if(Input.GetKeyDown(KeyCode.I))
			enableImportance.isOn = !enableImportance.isOn;

		//atalho para shadow
		if(Input.GetKeyDown(KeyCode.H))
			enableShadow.isOn = !enableShadow.isOn;

		//cicla os backgrounds
		if(Input.GetKeyDown(KeyCode.W))
			MoveBGIndex(1);

		if(Input.GetKeyDown(KeyCode.Q))
			MoveBGIndex(-1);

		if(Input.GetKeyDown(KeyCode.Tab))
			ShowHide();

		if(currentObj != null)
		{
			lastObj = currentObj;

			currentObj.SetTransparency(transparencySlider.value);

			txt_objSort.text = "Order: " + currentObj.GetSortOrder.ToString();

			if(!currentObj.properties.isLocked){

				Vector3 direction = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"),0);

				if(Input.GetKey(KeyCode.LeftShift) == false){
					currentObj.Move(direction);
				}
				else
				{
					Vector3 scale = currentObj.transform.localScale;
					scale.x += direction.x + direction.y;
					scale.y += direction.x + direction.y;

					if(scale.x > 0.2f && scale.x < 2)
						currentObj.transform.localScale = scale;
				}
			}
		}
		else
		{
			txt_objSort.text = string.Empty;
		}

		//Controla a camera de zoom
		if(isCameraZoomOn){
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			zoomCamera.transform.position = new Vector3(mousePos.x, mousePos.y, -10);
		}

		if(Input.GetKeyDown(KeyCode.C)){
			isCameraZoomOn = !isCameraZoomOn;
			zoomCamera.gameObject.SetActive(isCameraZoomOn);
		}

		//zoom da camera
		if(Input.GetKey(KeyCode.Equals))
			zoomCamera.orthographicSize -= 0.03f;

		if(Input.GetKey(KeyCode.Minus))
			zoomCamera.orthographicSize += 0.03f;

		//limita o size
		if(zoomCamera.orthographicSize <= 0.42f)
			zoomCamera.orthographicSize = 0.42f;
		if(zoomCamera.orthographicSize >= 1.9f)
			zoomCamera.orthographicSize = 1.9f;

		//--

	}

	void OnClick()
	{
		if(isTestingLevel)
			return;
		
		HiddenObject obj = OnScreenClick.instance.GetCurrentHiddenObject();

		if(obj == currentObj)
			return;

		if(currentObj != null)
			currentObj.SetTransparency(1);//volta a transparencia do obj antigo

		if(obj != null){
			currentObj = obj;
			ResetUI();

			//faz com o que o objeto na hud seja o primeiro da hierarquia
			currentObj.objectUI.transform.SetAsFirstSibling();
		}

	}

	void OnDestroy()
	{
		OnScreenClick.OnClick -= OnClick;
		ConfirmationWindow.OnWindowClosed -= OnWindowClosed;
		ConfirmationWindow.OnWindowRised -= OnWindowRised;
		GameManager.OnLevelLoaded -= OnLevelLoaded;
	}

	public void ChangeObjScale()
	{
		if(currentObj == null)
			return;
		
		float val = scaleSlider.value;
		Vector3 newScale = new Vector3(val, val, val);
		currentObj.objectUI.transform.localScale = newScale;
	}

	public void SortOrder(int val)
	{
		if(currentObj == null)
			return;

		currentObj.ChangeSortOrder(val);
	}


	/// <summary>
	/// Quando clica nos botoes prev e next. Enviar 1 ou -1
	/// </summary>
	/// <param name="direction">Direction.</param>
	public void MoveObjIndex(int direction)
	{
		objIndex += direction;

		if(objIndex < 0)
			objIndex = objectsList.Count -1;
		if(objIndex >= objectsList.Count)
			objIndex = 0;
		

		if(currentUIObj != null)
			Destroy(currentUIObj.gameObject);

		currentUIObj = Instantiate<HiddenObject>(objectsList[objIndex]);

		currentUIObj.transform.position = new Vector2(-50,-50);

		currentObj = null;

		//currentUIObj = objectsList[objIndex] as HiddenObject;
		currentObjImg.sprite = currentUIObj.GameSprite; //atualiza a imagem do obj
		scaleSlider.value = currentUIObj.transform.localScale.x;//atualiza o slider da escala
		txt_objName.text = currentUIObj.name;


	}

	/// <summary>
	/// Moves the index of the chap. Chamado pela UI
	/// </summary>
	/// <param name="direction">Direction.</param>
	public void MoveChapIndex(int direction)
	{
		int _chapIndex = chapIndex;
		List<HiddenObject> tempList = null;

		if(direction > 0)
		{
			for (int i = chapIndex; i < 100; i++) {

				chapIndex++;

				GameManager.hiddenObjectsLvlEditor.TryGetValue(chapIndex, out tempList);

				if(tempList != null){
					//chapIndex = i;
					break;
				}

			}

			//vai para o primeiro
			if(tempList == null)
			{
				chapIndex = 0;
				MoveChapIndex(1);
				return;
			}
		}
		else
		{
			for (int i = chapIndex; i >= 0; i--) {

				chapIndex--;

				GameManager.hiddenObjectsLvlEditor.TryGetValue(chapIndex, out tempList);

				if(tempList != null){
					//chapIndex = i;
					break;
				}

			}

			//vai para o último
			if(tempList == null)
			{
				chapIndex = 100;
				MoveChapIndex(-1);
				return;
			}
		}

		if(tempList != null){
			objectsList = new List<HiddenObject>(tempList);
			objIndex = -1;
			MoveObjIndex(1);
		}
		else{
			chapIndex = _chapIndex;

		}

	}

	public void MoveBGIndex(int direction)
	{
		Sprite bg = GameManager.instance.bgRenderer.sprite;

		//encontra o indice correto do BG
		for (int i = 0; i < GameManager.bgs.Count; i++) {
			if(GameManager.bgs[i] == bg){
				bgIndex = i;
				break;
			}
		}


		bgIndex += direction;

		if(bgIndex >= GameManager.bgs.Count)
			bgIndex = 0;
		if(bgIndex < 0)
			bgIndex = GameManager.bgs.Count-1;

		GameManager.instance.ChangeBG(bgIndex);

	}


	/// <summary>
	/// Spawna o objeto que esta na HUD do editor. Este objeto já estará na cena,
	/// porém a função Init dele ainda não foi chamada.
	/// </summary>
	public void SpawnObj()
	{
		if(currentUIObj == null)
			return;
		
		//cria uma cópia do objeto da UI
		//coloca o objeto no meio da tela
		HiddenObject h = Instantiate(currentUIObj, new Vector3(9.6f, 5.4f, 0), Quaternion.identity) as HiddenObject;

		h.Init();

		h.CreateUI();

		//atualiza hud
		currentObj = h;

		//instancia com sombra
		currentObj.EnableObjShadow(true);

		//iscliclable
		currentObj.EnableIsBoss(true);

		//Altera o layer sort order
		currentObj.ChangeSortOrder(6);

		GameManager.instance.AddHiddenObject(h);
	}

	void ResetUI()
	{
		enableImportance.isOn = currentObj.properties.isImportant;
		currentObjImg.sprite = currentObj.GameSprite; //atualiza a imagem do obj
		scaleSlider.value = currentObj.objectUI.transform.localScale.x;//atualiza o slider da escala
		txt_objName.text = currentObj.name;
		enableShadow.isOn = currentObj.properties.isShadowSpriteActive;
		enableIsBoss.isOn = currentObj.properties.isBoss;
		enableLock.isOn = currentObj.properties.isLocked;
		Toggle_enableBogg.isOn = currentObj.properties.isBogg;
	}


	/// <summary>
	/// Chamado pelo botão. Destroy o objeto selecionado.
	/// </summary>
	public void DestroyObj()
	{
		if(currentObj == null)
			return;

		Destroy(currentObj.gameObject);
	}

	public void ShowHide()
	{
//		if(mainRect.anchoredPosition.x < -10)
//			//não está escondido, esconde
//			mainRect.anchoredPosition = new Vector2(0, mainRectInitialPos.y);
//		else
//			mainRect.anchoredPosition = mainRectInitialPos;

		if(mainRectCanvasGroup.alpha >= 1){
		
			mainRectCanvasGroup.alpha = 0;
			mainRectCanvasGroup.interactable = false;
		}
		else
		{
			mainRectCanvasGroup.alpha = 1;
			mainRectCanvasGroup.interactable = true;
		}
	}

	public void UseShadow()
	{
		if(currentObj == null)
			return;

		currentObj.EnableObjShadow(enableShadow.isOn);
	}

	/// <summary>
	/// O objeto é importante para o level?
	/// </summary>
	public void ToggleIsImportant()
	{
		if(currentObj == null)
			return;

		currentObj.EnableImportance(enableImportance.isOn);
	}

	public void ToggleIsBogg()
	{
		if(currentObj == null)
			return;

		currentObj.EnableBogg(Toggle_enableBogg.isOn);

	}

	public void ToggleIsBoss()
	{
		if(currentObj == null)
			return;

		currentObj.EnableIsBoss(enableIsBoss.isOn);
	}

	public void Save()
	{
		if(levelID.text == string.Empty)
		{
			Debug.LogWarning("Level ID cannot be null");
			Notify("Level ID cannot be null");
			return;
		}
			
		//checa se o level exite
		if(SaveLoad.CheckLevel(levelID.text))
		{
			//Chama a caixa de confirmação
			ConfirmationWindow.instance.RiseWindow("Overwrite level "+levelID.text + "?", 
				()=>{
					UpdateAdvPanelValues();
					GameManager.instance.Save(levelID.text);
					Notify("Level " + levelID.text + " saved");
				}
			);

		}
		else
		{
			GameManager.instance.Save(levelID.text);
			Notify("Level " + levelID.text + " saved");
		}

		//GameManager.instance.Save(levelID.text);
		//Notify("Level " + levelID.text + " saved");
	}

	public void DeleteLevel()
	{
		//checa se esta nulo
		if(levelID.text == string.Empty)
		{
			Debug.LogWarning("Level ID cannot be null");
			Notify("Level ID cannot be null");
			return;
		}

		//checa se o level exite
		if(SaveLoad.CheckLevel(levelID.text))
		{
			//Chama a caixa de confirmação
			ConfirmationWindow.instance.RiseWindow("Delete level "+levelID.text + "?", 
				()=>{
				SaveLoad.DeleteLevel(levelID.text);
				Notify("Level " + levelID.text + " deleted");
			});

		}
		else
		{
			Notify("Level ID " + levelID.text + " doesn´t exist");
		}
	}

	public void Load()
	{
		if(levelID.text == string.Empty)
		{
			Debug.LogWarning("Level ID cannot be null");
			Notify("Level ID cannot be null");
			return;
		}

		GameManager.instance.Load(levelID.text, GameMode.EDITOR );
	}

	void OnLevelLoaded(bool success)
	{
		if(success){
			Notify("Level " + levelID.text + " loaded");
			UpdateAdvPnl();
		}
		else
			Notify("Level " + levelID.text + " doesn´t exist");
	}

	void UpdateAdvPnl()
	{
		lvlOnWinDropdown.captionText.text = GameManager.instance.currentLevelData.levelOnWin;

		lvlOnLoseDropdown.captionText.text = GameManager.instance.currentLevelData.levelOnLose;

		//----

		Dropdown_character.captionText.text = GameManager.instance.currentLevelData.character;

		//Diálogos

		InputField_introDialog.text = GameManager.instance.currentLevelData.introDialog.dialog;
		InputField_winDialog.text = GameManager.instance.currentLevelData.onWinDialog.dialog;
		InputField_defeatDialog.text = GameManager.instance.currentLevelData.onLoseDialog.dialog;

		//Lupas
		InputField_findsCount.text = GameManager.instance.currentLevelData.finds.ToString();
	}

	public void SetLock()
	{
		if(currentObj == null)
			return;

		currentObj.SetLocked(enableLock.isOn);

	}
	
	public void OnWindowRised()
	{
		mainRectCanvasGroup.interactable = false;
	}

	public void OnWindowClosed()
	{
		mainRectCanvasGroup.alpha = 1;
		mainRectCanvasGroup.interactable = true;
	}

	public void ChangeTransparency()
	{
		if(currentObj == null)
			return;
		
		currentObj.SetTransparency(transparencySlider.value);
	}


	/// <summary>
	/// Chamado pelo botão
	/// Testa o level atual	
	/// </summary>
	public void Test()
	{
		if(levelID.text == string.Empty)
		{
			Notify("Must load a level");
			return;
		}
		if(!SaveLoad.CheckLevel(levelID.text))
		{
			Notify("Level " + levelID.text + "doesn´t exist");
			return;
		}

		isTestingLevel = !isTestingLevel;

		GameMode gameMode = Toggle_timeAttack.isOn ? GameMode.GAME_TIMEATTACK : GameMode.GAME;


		if(isTestingLevel)
		{
			//carrega todo o level como se estivesse jogando

			//GameMode mode = isTimeAttack ? GameMode.GAME_TIMEATTACK : GameMode.GAME;

			GameManager.instance.Load(levelID.text, (GameMode)gameMode);
			btn_PlayStop.GetComponentInChildren<Text>().text = "Stop";
		}
		else
		{
			//carrega tudo novamente no editor
			Load();
			ShowHide();
			isCameraZoomOn = true;
			zoomCamera.gameObject.SetActive(isCameraZoomOn);
			btn_PlayStop.GetComponentInChildren<Text>().text = "Play";
			GameManager.instance.SetBlurEffect(false);
			GameUI.instance.TogglePauseMenu(false,0);
			GameManager.instance.SetPause(false);
			SoundManager.StopBGM();
		}
	}

	public void OpenPlayerPrefs()
	{
		string path = Application.persistentDataPath;
		System.Diagnostics.Process.Start(path);

	}
		
	public void OpenAdvPnl(bool open)
	{
		advLvlCanvasGroup.alpha = open ? 1 : 0;
		advLvlCanvasGroup.interactable = open;

		if(!open)
			return;

		if(GameManager.instance.currentLevelData == null){
			OpenAdvPnl(false);
			return;
		}

		List<string> lst = new List<string>();

		lst.Add("Menu");

		lst.AddRange(SaveLoad.GetAllLevels());

		lst.Sort();


		//Levels Win / Lose


		//preenche o dropdown
		lvlOnWinDropdown.ClearOptions();
		lvlOnWinDropdown.AddOptions(lst);

		lvlOnLoseDropdown.ClearOptions();
		lvlOnLoseDropdown.AddOptions(lst);

		lvlOnWinDropdown.captionText.text = GameManager.instance.currentLevelData.levelOnWin;

		lvlOnLoseDropdown.captionText.text = GameManager.instance.currentLevelData.levelOnLose;


		//----
	}

	/// <summary>
	/// Chamado pela UI
	/// </summary>
	public void OpenGeneralOptions()
	{
		
	}

	/// <summary>
	/// Chamado pela UI quando os valores do dropdown são alterados
	/// </summary>
	public void OnDropDwnVlwChanged()
	{
		if(GameManager.instance.currentLevelData == null)
			return;
		
		//Debug.Log("changed " + lvlOnLoseDropdown.captionText.text);
		GameManager.instance.currentLevelData.levelOnLose = lvlOnLoseDropdown.captionText.text;
		GameManager.instance.currentLevelData.levelOnWin = lvlOnWinDropdown.captionText.text;
	}

	/// <summary>
	/// Salva os valores do advanced panel
	/// </summary>
	public void UpdateAdvPanelValues()
	{
		if(GameManager.instance.currentLevelData == null)
			return;

		//GameManager.instance.currentLevelData.introDialog.character = Dropdown_character.captionText.text;
		//GameManager.instance.currentLevelData.onWinDialog.character = Dropdown_character.captionText.text;
		//GameManager.instance.currentLevelData.onLoseDialog.character = Dropdown_character.captionText.text;
		GameManager.instance.currentLevelData.character = Dropdown_character.captionText.text;

		GameManager.instance.currentLevelData.introDialog.dialog = InputField_introDialog.text;
		GameManager.instance.currentLevelData.onWinDialog.dialog = InputField_winDialog.text;
		GameManager.instance.currentLevelData.onLoseDialog.dialog = InputField_defeatDialog.text;

		//levels

		GameManager.instance.currentLevelData.levelOnWin = lvlOnWinDropdown.captionText.text;

		GameManager.instance.currentLevelData.levelOnLose = lvlOnLoseDropdown.captionText.text;

		//Lupa

		GameManager.instance.currentLevelData.finds = int.Parse(InputField_findsCount.text);

	}
}
