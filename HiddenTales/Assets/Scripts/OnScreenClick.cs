using UnityEngine;
using System.Collections;

public class OnScreenClick : MonoBehaviour {

	public static OnScreenClick instance;

	private HiddenObject currentHiddenObject;

	private Vector3 oldMousePos;
	private Vector3 newMousePos;

	public static Vector2 boxSize = new Vector2(0.7f, 0.7f);

	public static System.Action OnClick;

	void Awake()
	{
		instance = this;
	}

	// Use this for initialization
	void Start () {
		newMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		oldMousePos = newMousePos;
	}
	
	// Update is called once per frame
	void Update () {

		newMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

		if(Input.GetButtonDown("Fire1"))
		{
			
			//RaycastHit2D[] hit = Physics2D.RaycastAll(newMousePos, Camera.main.transform.forward);
			RaycastHit2D[] hit = Physics2D.BoxCastAll(newMousePos, boxSize, 0, Camera.main.transform.forward);

			GameManager.instance.OnClick(hit);

			if(hit.Length > 0){
				
				//temos que considerar o objeto mais próximo à frente de todos
				int maxOrder = 0;
				HiddenObject h = null;

				foreach (var item in hit) {

					HiddenObject ho = item.transform.GetComponent<HiddenObject>();

					if(ho == null)
						continue;

					if(ho.GetSortOrder >= maxOrder)
					{
						h = ho;
						maxOrder = ho.GetSortOrder;
					}

				}

				if(h != null){
					currentHiddenObject = h;
					//Debug.Log("Clicou no " + h.objName);
				}



			}

			//notifica
			if(OnClick != null)
				OnClick();


		}

		if(Input.GetButtonUp("Fire1"))
			currentHiddenObject = null;
		

		if(currentHiddenObject != null)
		{
			
			//arrasta o objeto
			Vector3 velocity = newMousePos - oldMousePos;

			currentHiddenObject.Move(velocity);
		
		}

		oldMousePos = newMousePos;
	
	}

	/// <summary>
	/// Obtém o objeto atual, usado pelo level editor
	/// </summary>
	/// <returns>The current hidden object.</returns>
	public HiddenObject GetCurrentHiddenObject()
	{
		return currentHiddenObject;
	}

	void OnDestroy()
	{
		instance = null;
	}



}
