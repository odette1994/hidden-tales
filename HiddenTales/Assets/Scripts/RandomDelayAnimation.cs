﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomDelayAnimation : MonoBehaviour
{
    Animator anim;


    private void OnEnable()
    {
        if( anim == null )
            anim = GetComponent<Animator>();
        anim.enabled = false;
        StartCoroutine(AnimDelay());
    }

    IEnumerator AnimDelay()
    {
        yield return new WaitForSeconds(Random.Range(0.01f,2.1f));
        anim.enabled = true;

    }


}
