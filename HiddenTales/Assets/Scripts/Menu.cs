﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Menu : MonoBehaviour {

	public CanvasGroup uiCanvasGroup;
	public static string levelToLoad;

	public RectTransform[] characters;
	public CanvasGroup swan;

	public CanvasGroup CanvasGroup_levelEditorButtons;

	public GameLevelStisticsData levelStatistics;

	public Text text_play;
	public GameObject bg;

	// Use this for initialization
	void Start () {

		//Previne o app entrar em standby
		Screen.sleepTimeout = SleepTimeout.NeverSleep;

		//Se a build estiver rodando em desktop ou editor, habilita o level editor
		#if !UNITY_EDITOR && !UNITY_STANDALONE

		CanvasGroup_levelEditorButtons.interactable = false;
		CanvasGroup_levelEditorButtons.blocksRaycasts = false;
		CanvasGroup_levelEditorButtons.alpha = 0;

		#endif

		//Obtem as informações de estrelas de cada level
		string data = PlayerPrefs.GetString("levels");
		levelStatistics = JsonUtility.FromJson<GameLevelStisticsData>(data);

		if (levelStatistics == null)
			levelStatistics = new GameLevelStisticsData();

		//todos os personagens somem
		for (int i = 0; i < characters.Length; i++) {
			LeanTween.alpha(characters[i], 0, 0);
		}

		LeanTween.alphaCanvas(swan, 0, 0);


		Fade.FadeIn(1,100, CheckCharacters);

		Vector3 origScale = bg.transform.localScale;

		//LeanTween.scale(bg, origScale*1.3f, 20f).setOnComplete(
		//	()=>{
		//		LeanTween.scale(bg, origScale, 20f);
		//	}
		
		//).setLoopPingPong();

		text_play.text = Dialogs.GetDialog ("play").text;

		//Toca BGM
		SoundManager.PlayBGM("ABERTURA");
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape)){
			Debug.Log("Quitting from menu");
			Application.Quit();
		}
	}

	/// <summary>
	/// Entra no level
	/// </summary>
	public void Play()
	{
		//TODO arrumar isso aqui
		GameManager.currentGameMode = GameMode.GAME;

		Fade.FadeOut(1, 100, () =>
		{
			UnityEngine.SceneManagement.SceneManager.LoadScene("LevelSelect");
		});

		uiCanvasGroup.interactable = false;

		SoundManager.PlaySFX("click and magic");
	}

	public static void LoadLevel(string levelID, GameMode gameMode)
	{
		levelToLoad = levelID;

		GameManager.currentGameMode = gameMode;
		UnityEngine.SceneManagement.SceneManager.LoadScene("Level");

	}

	/// <summary>
	/// Entra no level editor
	/// </summary>
	public void LoadEditor()
	{	
		GameManager.currentGameMode = GameMode.EDITOR;

		Fade.FadeOut(1, 100, ()=>{
			UnityEngine.SceneManagement.SceneManager.LoadScene("Level");
			Fade.FadeIn(1,100,null);
		});

		uiCanvasGroup.interactable = false;
	}

	public void ClearProgress()
	{
		PlayerPrefs.DeleteAll();
		Debug.Log("Deleted All Player Prefs");
	}

	//Abre a pasta data path no OS
	public void OpenPlayerPrefs()
	{
		string path = Application.persistentDataPath;
		System.Diagnostics.Process.Start(path);

	}

	/// <summary>
	/// Checa quais dos parsonagens devem aparecer
	/// </summary>
	void CheckCharacters()
	{

		levelStatistics.GetIsCompleateCharaterLevel(0);

		//Odette sempre aparece
		//Verifica se quem aparece é a Odette ou Swan
		//int odetteCount = levelStatistics.GetStarCount(4);
		//Debug.Log("CheckCharacters##################### odetteCount:"+ odetteCount);
		//if (odetteCount >= 15)
		if (PlayerPrefs.GetInt("complete_charactor_4",0)>0)
			LeanTween.alpha(characters[4], 1, 1);
		else
            LeanTween.alphaCanvas(swan, 1, 1);


		float delay = 0.5f;

		for (var i = 0; i < characters.Length; i++) {

			var starCount = levelStatistics.GetStarCount(i);


			//if(levelStatistics.GetStarCount(i) >= 15 && i != 4){
			if (PlayerPrefs.GetInt("complete_charactor_"+i, 0) > 0 && i != 4) { 
				LeanTween.alpha(characters[i], 1, 1).setDelay(delay);
				delay += 0.5f;
			}




		}




	}


}
