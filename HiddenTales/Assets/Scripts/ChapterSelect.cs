using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;

public class ChapterSelect : MonoBehaviour
{
    public static System.Action OnEnterLevelSelection;
    public static System.Action OnStartChapterComplete;
    public static ChapterSelect instance;

    static int currentSelecionIndex; //em qual grupo de persongens estamos?

    public CanvasGroup[] CanvasGroup_sections;
    public CanvasGroup CanvasGroup_stageSelection;

    public Text text_chapterselection;
    public Image image_logo;
    public RectTransform languageSelector;
    public Image image_animatedSwan;

    [HideInInspector]
    public List<Image> List_imagesToCopy = new List<Image>(); //lista de objetos para copiar a animação

    public Text textStarsCount;
    public GameObject nextPageButton;

    Characters character; //personagem selecionado nos capítulos
    int iLevelSelectCounter = 0;
    public ButtonChapterSelect[] chapters;
    public ButtonChapterSelect[] chaptersDlc;

    public RectTransform RectTransform_buttonPaidChapter;

    [Header("Stage Select")]
    public Image image_char;
    public Text text_characterTitle;
    public ButtonStageSelect[] ButtonStageSelect_list;
    public CanvasGroup CanvasGroup_hint;
    public Text text_hintMessage;

    //Levels
    [HideInInspector]
    public List<string> s_allLevels = new List<string>();
    [HideInInspector]
    public GameLevelStisticsData levelStatistics = new GameLevelStisticsData();

    [HideInInspector] public List<Sprite> sprite_boxes = new List<Sprite>();
    [HideInInspector]
    public string firstLevel;

    [Header("Chapter Completed")]
    public CanvasGroup CanvasGroup_chapterCompleted;
    public Image Image_chapterCompleteBG;
    public Sprite[] Sprite_CaractersEndingSprites;
    public static Characters ShowEndingForCharacter { get; private set; } //Personagem para mostar o final
    public static bool WillShowCharacterEnding { get; private set; }

    public static string secret_swan_num { get; set; }

    public GameObject unlockParticle;
    public GameObject unlockArrowPaidParticle;

    public bool isCanClickButtonChapter = true;

    private AsyncOperationHandle<Sprite> _loadedBigChar;
    private AsyncOperationHandle<IList<Sprite>> _loadedBoxes;

    void Awake()
    {
        instance = this;

        //Obtem as informações de estrelas de cada level
        string data = PlayerPrefs.GetString("levels");
        levelStatistics = JsonUtility.FromJson<GameLevelStisticsData>(data);

        if (levelStatistics == null)
            levelStatistics = new GameLevelStisticsData();

        LoadBoxes();

        CanvasGroup_hint.alpha = 0;
        CanvasGroup_hint.blocksRaycasts = false;

    }

    private void OnDisable()
    {
        ReleaseAllAddressables();
    }

    private void LoadBoxes()
    {
        Addressables.LoadAssetsAsync<Sprite>("ChapterBoxes", null).Completed += BoxesLoaded;
    }

    private void BoxesLoaded(AsyncOperationHandle<IList<Sprite>> obj)
    {
        _loadedBoxes = obj;
        foreach (var spr in _loadedBoxes.Result)
            sprite_boxes.Add(spr);
        Debug.Log("<Color=Blue>Loaded all SpriteBox sprites. Updating all buttons</Color>");
        UpdateAllButtonBGs();
    }

    private void UpdateAllButtonBGs()
    {
        for (int i = 0; i < ButtonStageSelect_list.Length; ++i)
        {
            ButtonStageSelect_list[i].UpdateBoxBG();
        }
    }

    public Sprite GetSpriteBox(string index)
    {
        foreach (var item in sprite_boxes)
        {
            if (item.name.Contains(index))
                return item;
        }
        Debug.LogError("Couldn't find proper SpriteBox. Returning null");
        return null;
    }

    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < chaptersDlc.Length; i++)
        {
            chaptersDlc[i].isDLC = true;
        }

        CalculateStars();

        Fade.FadeIn(1.5f);
        //Evento para saber que o usuário chegou na selecao de capitulos
        //UniRate.Instance.LogEvent (true);
        text_chapterselection.text = Dialogs.GetDialog("selectchapter").text;

        iLevelSelectCounter = PlayerPrefs.GetInt("LevelSelectionCounter");
        if (iLevelSelectCounter == 3)
        {
            /*			MobileNativeRateUs ratePopUp = new MobileNativeRateUs ("Do you like Swan Princess?", "Please rate to support future updates!", "yes", "later", "no");
                        ratePopUp.OnComplete += OnRatePopUpClose;
                        #if UNITY_ANDROID
                        ratePopUp.SetAndroidAppUrl("https://play.google.com/store/apps/details?id=com.swanprincess.hiddenobjects");
                        #elif
                        ratePopUp.SetAppleId ("1160114092");
                        #endif
            */
        }
        iLevelSelectCounter = iLevelSelectCounter + 1;
        PlayerPrefs.SetInt("LevelSelectionCounter", iLevelSelectCounter);
        iLevelSelectCounter = PlayerPrefs.GetInt("LevelSelectionCounter");
        Debug.Log("iLevelSelectCounter3: " + iLevelSelectCounter);

        //Toca o BGM apenas se não estiver tocando o tema da abertura
        SoundManager.PlayBGM("ABERTURA");

        //Ativa a seleção dos personagens
        EnableCanvasGroup(currentSelecionIndex);

        CanvasGroup_stageSelection.alpha = 0;
        CanvasGroup_stageSelection.blocksRaycasts = false;

        LeanTween.alpha(image_char.GetComponent<RectTransform>(), 0, 0f);

        s_allLevels.AddRange(SaveLoad.GetAllLevels());
        s_allLevels.Sort();

        //InAppHandler.Instance.onStartIAP();

        //Anima o botão paid
        LeanTween.move(RectTransform_buttonPaidChapter, new Vector3(774, -229.8f, 0), 0.8f).setLoopPingPong().setEaseInOutSine();

        if (WillShowCharacterEnding)
        {
            StartCoroutine(ChapterCompletedCo());
        }
    }

    public void CalculateStars()
    {
        //minimumStars 0 6 12 24 48  DLC = 96

        int starsCount = (levelStatistics != null) ? levelStatistics.GetStarCount() : 0;
        var totalNeeded = 0;

        Debug.Log("------ CalculateStars ##############################\n");
        Debug.Log("starsCount:"+ starsCount);

        // New unlock rule
        int remainCountUnlockNomalChapter = chapters.Length;
        int usedSwan = 0;
        int starSwanUse = 0;

        Debug.Log("------ chapters.Length:" + chapters.Length + "\n");
        for (int i = 0; i < chapters.Length; i++)
        {
            if (chapters[i].minimumStars == 0 )
            {
                chapters[i].SetEnable(true);
                remainCountUnlockNomalChapter--;
            }else if (PlayerPrefs.GetInt(chapters[i].chapterStorageName+"_unlocked_by_swan",0) >= 1  )
            {
                starSwanUse = PlayerPrefs.GetInt(chapters[i].chapterStorageName + "_unlocked_by_swan");
                usedSwan += starSwanUse;
                Debug.Log("PlayerPrefs starSwanUse: " + chapters[i].chapterStorageName + " :" + starSwanUse);
                chapters[i].SetEnable(true);
                remainCountUnlockNomalChapter--;
            }
            else
                chapters[i].SetEnable(false);

        }

        Debug.Log("------ chaptersDlc.Length:" + chaptersDlc.Length + "\n");

        for (int i = 0; i < chaptersDlc.Length; i++)
        {

            Debug.Log("Testing chapter: " + chaptersDlc[i].chapterStorageName +" = "+PlayerPrefs.GetInt(chaptersDlc[i].chapterStorageName));
            if (PlayerPrefs.GetInt(chaptersDlc[i].chapterStorageName) > 0)
            {
                Debug.Log("Enabling chapter " + chaptersDlc[i].chapterStorageName);
                chaptersDlc[i].SetEnableDLC(true);
            }
            else if (PlayerPrefs.GetInt(chaptersDlc[i].chapterStorageName + "_unlocked_by_swan", 0) >= 1)
            {
                starSwanUse = PlayerPrefs.GetInt(chaptersDlc[i].chapterStorageName + "_unlocked_by_swan");
                usedSwan += starSwanUse;
                Debug.Log("PlayerPrefs starSwanUse: " + chaptersDlc[i].chapterStorageName + " :" + starSwanUse);

                chaptersDlc[i].SetEnableDLC(true);

                Debug.Log("usedSwan: " + chaptersDlc[i].chapterStorageName + " :" + usedSwan);
            }
            else
                chaptersDlc[i].SetEnableDLC(false);
        }

        Debug.Log("usedSwan :" + usedSwan);

        int current = starsCount - usedSwan;

        Debug.Log("current:"+ current);
        SetSecretSwanFromInt(current);

        textStarsCount.text = string.Format("x{0}", current.ToString());
    }
    
    /*	//Rate this App function
        private void OnRatePopUpClose(MNDialogResult result)
        {
            switch(result)
            {
            case MNDialogResult.RATED:

                break;
            case MNDialogResult.REMIND:
                PlayerPrefs.SetInt("LevelSelectionCounter",-2);
                break;
            case MNDialogResult.DECLINED:
                PlayerPrefs.SetInt("LevelSelectionCounter",-10);
                break;
            }
        }
    */

    IEnumerator ChapterCompletedCo()
    {
        WillShowCharacterEnding = true;

        //TODO:: Clean code
        //Show CanvasGroup
        // [ Level Select Button 1-5 ]  ( index 0 - 4 ShowEndingForCharacter )
        // [ Level Select Button 6-8 ]  ( index 5 - 7 ShowEndingForCharacter )
        // [ Level Select Button 9-11 ] ( index 8 - 10 ShowEndingForCharacter )
        if ((int)ShowEndingForCharacter > 7)
        {
            EnableCanvasGroup(2, 0.3f);
        }
        else if ((int)ShowEndingForCharacter > 4)
        {
            EnableCanvasGroup(1, 0.3f);
        }
        //else EnableCanvasGroup index 0 still on screen


        yield return new WaitForSeconds(.7f);
        if (OnStartChapterComplete != null)
            OnStartChapterComplete();

        yield return new WaitForSeconds(1.5f);
        Image_chapterCompleteBG.sprite = Sprite_CaractersEndingSprites[(int)ShowEndingForCharacter];
        
        LeanTween.scale(Image_chapterCompleteBG.rectTransform, Vector3.zero, 0);
        yield return new WaitForEndOfFrame();
        LeanTween.alphaCanvas(CanvasGroup_chapterCompleted, 1, 1);
        yield return new WaitForSeconds(1);
        LeanTween.scale(Image_chapterCompleteBG.rectTransform, Vector3.one, 0.4f).setEaseOutCubic();
        yield return new WaitForSeconds(6);
        LeanTween.scale(Image_chapterCompleteBG.rectTransform, Vector3.zero, 0.4f).setEaseOutCubic();
        yield return new WaitForSeconds(1);
        LeanTween.alphaCanvas(CanvasGroup_chapterCompleted, 0, 1);
        yield return new WaitForSeconds(1);
        WillShowCharacterEnding = false;
        yield return null;
    }

    // Update is called once per frame
    void Update()
    {
        //Força a liguagem
        if (Input.GetKeyDown(KeyCode.Alpha1))
            Dialogs.SetLanguage(Languages.English);

        if (Input.GetKeyDown(KeyCode.Alpha2))
            Dialogs.SetLanguage(Languages.Portuguese);

        if (Input.GetKeyDown(KeyCode.Alpha3))
            Dialogs.SetLanguage(Languages.German);

        if(Input.GetKeyDown(KeyCode.Alpha4))
            Dialogs.SetLanguage(Languages.Spanish);
        
        //Copia as imagens do cisne para as animações
        for (int i = 0; i < List_imagesToCopy.Count; i++)
        {
            List_imagesToCopy[i].sprite = image_animatedSwan.sprite;
        }
    }

    public void EnableCanvasGroup(int index, float time)
    {

        currentSelecionIndex = index;

        for (int i = 0; i < CanvasGroup_sections.Length; i++)
        {

            if (i == currentSelecionIndex)
            {
                CanvasGroup_sections[i].interactable = true;
                CanvasGroup_sections[i].blocksRaycasts = true;
                LeanTween.alphaCanvas(CanvasGroup_sections[i], 1, time);
            }
            else
            {
                CanvasGroup_sections[i].interactable = false;
                CanvasGroup_sections[i].blocksRaycasts = false;
                LeanTween.alphaCanvas(CanvasGroup_sections[i], 0, time);
            }
        }
    }

    public void EnableCanvasGroup(int index)
    {
        if (WillShowCharacterEnding)
            return;

        currentSelecionIndex = index;

        for (int i = 0; i < CanvasGroup_sections.Length; i++)
        {

            LeanTween.cancel(CanvasGroup_sections[i].gameObject);

            if (i == currentSelecionIndex)
            {
                CanvasGroup_sections[i].interactable = true;
                CanvasGroup_sections[i].blocksRaycasts = true;
                CanvasGroup_sections[i].alpha = 1;
            }
            else
            {
                CanvasGroup_sections[i].interactable = false;
                CanvasGroup_sections[i].blocksRaycasts = false;
                CanvasGroup_sections[i].alpha = 0;
            }
        }

    }

    /// <summary>
    /// Deixa o canvas group não interativo quando seleciona um capitulo
    /// </summary>
    /// <param name="index">Index.</param>
    public void DisableCurrentCanvasGroup()
    {
        for (int i = 0; i < CanvasGroup_sections.Length; i++)
        {
            if (i == currentSelecionIndex)
            {
                CanvasGroup_sections[i].interactable = false;
                CanvasGroup_sections[i].blocksRaycasts = false;
            }
        }
    }

    /// <summary>
    /// Desabilita os paineis de botoes de personagens
    /// </summary>
    public void DisableAllCanvasGroup()
    {
        for (int i = 0; i < CanvasGroup_sections.Length; i++)
        {
            LeanTween.alphaCanvas(CanvasGroup_sections[i], 0, 0.5f);
            CanvasGroup_sections[i].interactable = false;
            CanvasGroup_sections[i].blocksRaycasts = false;
        }
    }

    public void EnterLevel(string level, GameMode gameMode)
    {
        //desabilita todos os botoes
        DisableCurrentCanvasGroup();
        CanvasGroup_stageSelection.blocksRaycasts = false;
        GameManager.storedTotalStarsCount = levelStatistics.GetStarCount((int)character);
        SoundManager.PlaySFX("click and magic");
        SoundManager.StopBGM();
        Fade.FadeOut(1.7f, 100,
            () =>
            {
                Menu.LoadLevel(level, gameMode);
            }
        );
    }

    public void EnterStageSelection(Characters character, string firstLevel)
    {
        DisableAllCanvasGroup();

        //Aparecem logos e titulos
        LeanTween.alpha(image_logo.GetComponent<RectTransform>(), 0, 0.5f);
        LeanTween.alpha(text_chapterselection.GetComponent<RectTransform>(), 0, 0.5f);
        LeanTween.alpha(languageSelector, 0, 0.5f);

        this.character = character;
        this.firstLevel = firstLevel;

        //Formata o nome do Personagem
        Debug.Log("<Color=Blue>About to load character</Color>");
        text_characterTitle.text = Utils.GetNiceCharacterName(character);
        Addressables.LoadAssetAsync<Sprite>(character.ToString()).Completed += BigCharacterLoaded;

        LeanTween.alphaCanvas(CanvasGroup_stageSelection, 1, 1).setOnComplete(
            () =>
            {
                LeanTween.alpha(image_char.GetComponent<RectTransform>(), 1, 0.3f);
            });
        CanvasGroup_stageSelection.blocksRaycasts = true;
        LeanTween.alpha(text_characterTitle.GetComponent<RectTransform>(), 1, 1f);

        if (OnEnterLevelSelection != null)
            OnEnterLevelSelection(); //Evento
    }

    private void BigCharacterLoaded(AsyncOperationHandle<Sprite> obj)
    {
        _loadedBigChar = obj;
        Debug.Log($"Loaded sprite {obj.Result}");
        image_char.sprite = obj.Result;
    }

    private void ReleaseAllAddressables()
    {
        Addressables.Release(_loadedBigChar);
        Addressables.Release(_loadedBoxes);
    }

    private void ReleaseBigCharAddressables()
    {

        Addressables.Release(_loadedBigChar);
    }
    
    /// <summary>
    /// Cancela a selecao de estagio e volta para a tela de personagens
    /// </summary>
    /// <returns><c>true</c> if this instance cancel stage selection; otherwise, <c>false</c>.</returns>
    public void CancelStageSelection()
    {
        LeanTween.alpha(image_char.GetComponent<RectTransform>(), 0, 0.3f).setOnComplete(
            () =>
            {
                LeanTween.alphaCanvas(CanvasGroup_stageSelection, 0, 0.5f).setOnComplete(
                    () => { CanvasGroup_stageSelection.blocksRaycasts = false; });

                LeanTween.alpha(text_characterTitle.GetComponent<RectTransform>(), 0, 0.5f);
                LeanTween.alpha(image_logo.GetComponent<RectTransform>(), 1, 0.5f);
                LeanTween.alpha(text_chapterselection.GetComponent<RectTransform>(), 1, 0.5f);
                LeanTween.alpha(languageSelector, 1, 0.5f);
                EnableCanvasGroup(currentSelecionIndex, 1);
                ReleaseBigCharAddressables();
            });
    }

    /// <summary>
    /// Chamado pelo botão PLAY CHAPTER
    /// </summary>
    public void PlayChapter()
    {
        EnterLevel(firstLevel, GameMode.GAME);
    }

    public void PlayChapterEasy()
    {
        EnterLevel(firstLevel, GameMode.GAME_EASY);
    }

    public string GetFullLevelName(string lvlName)
    {
        foreach (var item in s_allLevels)
        {
            if (item.Contains(lvlName))
                return item;
        }

        return string.Empty;
    }

    //Chamado pelo botão. Fecha a janela de hint
    public void CloseHint()
    {
        Debug.Log("Hint closed");
        LeanTween.alphaCanvas(CanvasGroup_hint, 0, 0.5f);
        CanvasGroup_hint.blocksRaycasts = false;
    }

    public void OpenHint(string message)
    {
        text_hintMessage.text = message;

        LeanTween.alphaCanvas(CanvasGroup_hint, 1, 0.5f);
        CanvasGroup_hint.blocksRaycasts = true;
    }

    public static void ShowCompletedLevel(Characters character)
    {
        ShowEndingForCharacter = character;
        WillShowCharacterEnding = true;
    }

    public void SetSecretSwanFromInt(int swan_num)
    {
        secret_swan_num = Crypt(swan_num.ToString());
    }

    public int GetSecretSwan()
    {
        return int.Parse(Derypt(secret_swan_num));
    }


    public static string Crypt(string str)
    {
        return Convert.ToBase64String(System.Text.Encoding.Unicode.GetBytes(str));
    }

    public static string Derypt(string str)
    {
        return System.Text.Encoding.Unicode.GetString(Convert.FromBase64String(str));
    }

    public void PlayUnlockParticleAtPoint(Vector3 v3)
    {
        unlockParticle.SetActive(false);
        unlockParticle.transform.position = new Vector3(v3.x, v3.y, unlockParticle.transform.position.z);
        unlockParticle.SetActive(true);
        SoundManager.PlaySFX("click and magic");
    }

    IEnumerator ShowFirstArrow()
    {
        yield return new WaitForSeconds(.5f);
        RectTransform rt = RectTransform_buttonPaidChapter; 
        Canvas cv = rt.root.GetComponent<Canvas>();
        Vector2 v2 = rt.anchoredPosition;
        RectTransform rootRtLevel1 = RectTransform_buttonPaidChapter.transform.parent.GetComponent<RectTransform>();
        double scaledScreenW = Screen.width / cv.scaleFactor;
        double addX = (scaledScreenW * .5f) - (rootRtLevel1.rect.width * .5f) + rootRtLevel1.anchoredPosition.x;
        double vxD = (v2.x + addX + rt.rect.width * .5f) * cv.scaleFactor;
        Vector3 v3 = Camera.main.ScreenToWorldPoint(new Vector3(float.Parse(vxD.ToString()), 0, 0));
        unlockArrowPaidParticle.SetActive(false);
        unlockArrowPaidParticle.transform.position = new Vector3(v3.x, -0.38f, unlockArrowPaidParticle.transform.position.z);
        unlockArrowPaidParticle.SetActive(true);
        SoundManager.PlaySFX("sfx02");
        yield return new WaitForSeconds(.3f);
        nextPageButton.SetActive(true);
    }



}
