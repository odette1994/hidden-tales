[Settings]

#Tempo limite em segundos
Time_Limit = 120

Time_Limit_Easy_Mode = 900

#objeto pisca depois de xx segundos depois do ultimo ser encontrado
Blink_After = 15

#numero minimo de objetos clicaveis
Minimum_Objects = 12

#Percentagem de objetos importantes 
Importants_Objects_Percentage = 25

#desconto de tempo quando erra no modo normal
Time_Penalty_Error = 5

Time_Penalty_Error_EasyMode = 2

#desconto de tempo quando erra no modo time attack
Time_Penalty_Error_TimeAttack = 5

#numero de finds da lupa
Finds = 0

#Boggs

#Tempo para encontrar o boog
time_limit_to_find_bogg = 15

#Primeiro Bogg
first_bogg_appears_after_objects = 4
first_bogg_appears_after_seconds = 35

#Segundo Bogg
second_bogg_appears_after_objects = 9
second_bogg_appears_after_seconds = 65
